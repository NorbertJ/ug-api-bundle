<?php
namespace ApiBundle\Form;

use ApiBundle\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * @author Norbert Jurkiewicz <norbert.jurkiewicz@gmail.com>
 */
class PublicationForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('authors', EntityType::class, [
            'class' => 'ApiBundle:User',
            'choice_label' => function (User $user) {

                return $user->getFirstName() . ' ' . $user->getLastName();
            },
            'multiple' => true,
            'expanded' => true,
            'query_builder' => function (EntityRepository $er) {
                $qb = $er
                        ->createQueryBuilder('a')
                        ->join('a.userRoles', 'b')
                        ->andWhere('b.role = :rolePerson')
                        ->setParameter('rolePerson', 'ROLE_PERSON')
                ;

                return $qb;
            },
        ]);
        $builder->add('year');
        $builder->add('title');
        $builder->add('magazine');
        $builder->add('pageNumber');
        $builder->add('pdf');
        $builder->add('points');
    }
    
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'csrf_protection' => false,
        ]);
    }

}

