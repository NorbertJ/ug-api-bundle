<?php
namespace ApiBundle\Form;

use ApiBundle\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * @author Norbert Jurkiewicz <norbert.jurkiewicz@gmail.com>
 */
class InventoryForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('invoice', EntityType::class, [
            'class' => 'ApiBundle:Invoice',
            'choice_label' => 'description',
        ]);
        $builder->add('value', MoneyType::class);
        $builder->add('type');
        $builder->add('isArchived');
        $builder->add('serialNumber');
        $builder->add('productionDate', DateType::class, ['widget' => 'single_text']);
        $builder->add('inventoryNumber');
        $builder->add('barCode');
        $builder->add('usePlace');
        $builder->add('responsiblePerson', EntityType::class, [
            'class' => 'ApiBundle:User',
            'choice_label' => function (User $user) {

                return $user->getFirstName() . ' ' . $user->getLastName();
            },
            'query_builder' => function (EntityRepository $er) {
                $qb = $er
                        ->createQueryBuilder('a')
                        ->join('a.userRoles', 'b')
                        ->andWhere('b.role = :rolePerson')
                        ->setParameter('rolePerson', 'ROLE_WORKER')
                ;

                return $qb;
            },
        ]);
        $builder->add('storagePlace', EntityType::class, [
            'class' => 'ApiBundle:StoragePlace',
            'choice_label' => 'name',
        ]);
        $builder->add('aparatureOrMaterial');
        $builder->add('owner', EntityType::class, [
            'class' => 'ApiBundle:User',
            'choice_label' => function(User $user)
            {
                return $user->getFirstName(). ' '. $user->getLastName();
            },
            'multiple' => false
        ]);
        $builder->add('isBorrowed');
        $builder->add('returnDate', DateType::class, ['widget' => 'single_text']);

    }
    
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'csrf_protection' => false,
        ]);
    }

}

