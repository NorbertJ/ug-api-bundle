<?php
namespace ApiBundle\Form;

use ApiBundle\ApiBundle;
use ApiBundle\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;

/**
 * @author Norbert Jurkiewicz <norbert.jurkiewicz@gmail.com>
 */
class BookForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('authors', EntityType::class, [
            'class' => 'ApiBundle:User',
            'choice_label' => function (User $user) {

                return $user->getFirstName() . ' ' . $user->getLastName();
            },
            'multiple' => true,
            'expanded' => true,
            'query_builder' => function (EntityRepository $er) {
                $qb = $er
                        ->createQueryBuilder('a')
                        ->join('a.userRoles', 'b')
                        ->andWhere('b.role = :rolePerson')
                        ->setParameter('rolePerson', 'ROLE_PERSON')
                ;

                return $qb;
            },
        ]);
        $builder->add('year');
        $builder->add('title');
        $builder->add('publisher');
        $builder->add('isbn');
        $builder->add('bookstand');
        $builder->add('shelf');
        $builder->add('id');
        $builder->add('owner', EntityType::class, [
            'class' => 'ApiBundle:User',
            'choice_label' => function(User $user)
            {
                return $user->getFirstName(). ' '. $user->getLastName();
            },
            'multiple' => false
        ]);
        $builder->add('isBorrowed');
        $builder->add('barCode');
        $builder->add('returnDate', DateType::class, ['widget' => 'single_text']);

    }
    
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'csrf_protection' => false,
        ]);
    }

}

