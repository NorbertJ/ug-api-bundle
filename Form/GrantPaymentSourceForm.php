<?php
namespace ApiBundle\Form;

use ApiBundle\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * @author Norbert Jurkiewicz <norbert.jurkiewicz@gmail.com>
 */
class GrantPaymentSourceForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('contractors', EntityType::class, [
            'class' => 'ApiBundle:User',
            'choice_label' => function (User $user) {

                return $user->getFirstName() . ' ' . $user->getLastName();
            },
            'multiple' => true,
            'expanded' => true,
            'query_builder' => function (EntityRepository $er) {
                $qb = $er
                        ->createQueryBuilder('a')
                        ->join('a.userRoles', 'b')
                        ->andWhere('b.role = :rolePerson')
                        ->setParameter('rolePerson', 'ROLE_PERSON')
                ;

                return $qb;
            },
        ]);
            
        $builder->add('manager', EntityType::class, [
            'class' => 'ApiBundle:User',
            'choice_label' => function (User $user) {

                return $user->getFirstName() . ' ' . $user->getLastName();
            },
            'query_builder' => function (EntityRepository $er) {
                $qb = $er
                        ->createQueryBuilder('a')
                        ->join('a.userRoles', 'b')
                        ->andWhere('b.role = :rolePerson')
                        ->setParameter('rolePerson', 'ROLE_PERSON')
                ;

                return $qb;
            },
        ]);
        $builder->add('year');
        $builder->add('isDs');
        $builder->add('name');
        $builder->add('totalValue', MoneyType::class);
        $builder->add('isGrant');
        $builder->add('description');
        $builder->add('fromDate', DateType::class, ['widget' => 'single_text']);
        $builder->add('number');
        $builder->add('source');
        $builder->add('title');
        $builder->add('toDate', DateType::class, ['widget' => 'single_text']);
        $builder->add('invoice', EntityType::class, [
            'class' => 'ApiBundle:Invoice',
            'choice_label' => 'description',
        ]);

    }
    
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'csrf_protection' => false,
        ]);
    }

}

