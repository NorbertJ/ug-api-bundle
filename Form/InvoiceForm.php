<?php
namespace ApiBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * @author Norbert Jurkiewicz <norbert.jurkiewicz@gmail.com>
 */
class InvoiceForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('company', EntityType::class, [
            'class' => 'ApiBundle:Company',
            'choice_label' => 'name',
        ]);
        $builder->add('date', DateType::class, ['widget' => 'single_text']);
        $builder->add('number');
        $builder->add('description');
        $builder->add('grossValue', MoneyType::class);
        $builder->add('material');
        $builder->add('category');
    }
    
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'csrf_protection' => false,
            'isEdit' => false,
        ]);
    }

}

