<?php
namespace ApiBundle\Form;

use ApiBundle\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BaseType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * @author Norbert Jurkiewicz <norbert.jurkiewicz@gmail.com>
 */
class UserForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options) {
        
        $passwordOptions = [
            'first_name' => 'password',
            'second_name' => 'confirm',
            'type' => PasswordType::class,
        ];
        if (!$options['isEdit']) {
            $passwordOptions['constraints'] = [new NotBlank()];
        }
        $builder->add('plainPassword', RepeatedType::class, $passwordOptions);
        $builder->add('email', EmailType::class);
        $builder->add('userRoles', EntityType::class, [
            'class'=>'ApiBundle:UserRole',
            'choice_label'=>'role',
            'multiple' => true,
            'expanded' => true,
        ]);
        $builder->add('isActive');
        $builder->add('firstName');
        $builder->add('lastName');
        $builder->add('pesel');
        $builder->add('birthDate', DateType::class, ['widget' => 'single_text']);
        $builder->add('workerNumber');
        $builder->add('workingSinceDate', DateType::class, ['widget' => 'single_text']);
        $builder->add('physicianTestsValidDate', DateType::class, ['widget' => 'single_text']);
        $builder->add('position');
        $builder->add('address');
        $builder->add('isConsulting');
        $builder->add('consultationDate', DateType::class, ['widget' => 'single_text']);
        $builder->add('bankAccountNumber');
        $builder->add('phoneNumber');
        $builder->add('roomNumber');
        $builder->add('lab');
        $builder->add('title');
        $builder->add('promotors', EntityType::class, [
            'class' => 'ApiBundle:User',
            'choice_label' => function (User $user) {

                return $user->getFirstName() . ' ' . $user->getLastName();
            },
            'multiple' => true,
            'expanded' => true,
            'query_builder' => function (EntityRepository $er) {
                $qb = $er
                        ->createQueryBuilder('a')
                        ->join('a.userRoles', 'b')
                        ->andWhere('b.role = :rolePerson')
                        ->setParameter('rolePerson', 'ROLE_PERSON')
                ;

                return $qb;
            },
        ]);
        $builder->add('fieldOfStudies');
        $builder->add('typeOfStudies', EntityType::class, [
            'class' => 'ApiBundle:StudiesType',
            'choice_label' => 'name']
        );
        
    }
    
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'csrf_protection' => false,
            'isEdit' => false,
        ]);
    }

}

