<?php
/**
 * File Description
 *
 * Some comprehensive Description of the files contents
 *
 * @author Norbert Jurkiewicz <norbert.jurkiewicz@interactivedata.com>
 * @user norbert
 * @version $Author:$ $Id:$ $Revision:$ $Date:$
 * @since 09.10.2016
 */

namespace ApiBundle\Form;

use ApiBundle\Entity\GrantPaymentSource;
use ApiBundle\Entity\Invoice;
use ApiBundle\Entity\IncomeOutcome;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class IncomeOutcomeForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('value', MoneyType::class);
        $builder->add('description');
        $builder->add('grantpaymentsource', EntityType::class, [
            'class' => 'ApiBundle\Entity\GrantPaymentSource',
            'choice_label' => 'title'
        ]);
        $builder->add('invoice', EntityType::class, [
            'class' => 'ApiBundle\Entity\Invoice',
            'choice_label' => 'number'
        ]);
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'csrf_protection' => false,
        ]);
    }
}