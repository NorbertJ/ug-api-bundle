<?php
namespace ApiBundle\ORM\DataFixtures;

use ApiBundle\Entity\StudiesType;
use ApiBundle\Entity\User;
use ApiBundle\Entity\UserRole;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * 
 * Load masters and licentiates
 *
 * @author Norbert Jurkiewicz <norbert.jurkiewicz@gmail.com>
 */
class LoadUser2 extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface {
    
    /**
     * @var ContainerInterface
     */
    private $container;
    
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager) {
        //[email, password, [role index], first_name, second_name, title, [promotors - user index], field_of_studies, type_of_studies index]
        $sampleUsers = [
            ['master@just-code.it', 'master', [6], 'Mastername', 'Mastersurname', 'title of master publication', [3,5], 'chemistry, drugs and explosives', 0],
            ['licentiate@just-code.it', 'licentiate', [7], 'Licentiatename', 'Licentiatesurname', 'title of licentiate publication', [4,6,7], 'turfgrass science', 1],
        ];
        
        $i = 0;
        foreach ($sampleUsers as $userData) {
            $user = new User();
            $user->setEmail($userData[0]);
            $encoder = $this->container->get('security.password_encoder');
            $user->setPassword($encoder->encodePassword($user, $userData[1]));
            foreach ($userData[2] as $roleIndex) {
                $user->addUserRole($this->getReference(UserRole::class . DIRECTORY_SEPARATOR . $roleIndex));
            }
            $user->setFirstName($userData[3]);
            $user->setLastName($userData[4]);
            //add master/licentiate data
            $user->setTitle($userData[5]);
            foreach($userData[6] as $userIndex){
                $user->addPromotor($this->getReference(User::class . DIRECTORY_SEPARATOR . $userIndex));
            }
            $user->setFieldOfStudies($userData[7]);
            $user->setTypeOfStudies($this->getReference(StudiesType::class . DIRECTORY_SEPARATOR . $userData[8]));

            $manager->persist($user);
        }

        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
    
    public function getOrder() {
        return 4;
    }
    
}
