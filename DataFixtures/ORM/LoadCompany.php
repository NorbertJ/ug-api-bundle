<?php
namespace ApiBundle\ORM\DataFixtures;

use ApiBundle\Entity\Company;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Load client
 *
 * @author Norbert Jurkiewicz <norbert.jurkiewicz@gmail.com>
 */
class LoadCompany extends AbstractFixture implements OrderedFixtureInterface {
    
    
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        //[nip, name, address]
        $sampleData = [
            ['123-456-45-45', 'Company 1', 'Address 1 street 1 wfewewfew'],
            ['123-456-45-46', 'Company 2', 'Address 2 street 2 wfewewfew'],
            ['123-456-45-47', 'Company 3', 'Address 3 street 3 wfewewfew'],
            ['123-456-45-48', 'Company 4', 'Address 4 street 4 wfewewfew'],
            ['123-456-45-49', 'Company 5', 'Address 5 street 5 wfewewfew'],
            ['123-456-45-22', 'Company 6', 'Address 5 street 5 wfewewfew'],
        ];
        
        $i = 0;
        foreach($sampleData as $data){
            $company = new Company();
            $company->setAddress($data[0]);
            $company->setName($data[1]);
            $company->setNip($data[2]);
            $this->addReference(get_class($company).DIRECTORY_SEPARATOR.$i++, $company);
            
            $manager->persist($company);
        }

        $manager->flush();
    }
    
    public function getOrder() {
        return 11;
    }
    
}
