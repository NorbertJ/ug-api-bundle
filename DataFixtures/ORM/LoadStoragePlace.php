<?php
namespace ApiBundle\ORM\DataFixtures;

use ApiBundle\Entity\StoragePlace;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Load client
 *
 * @author Norbert Jurkiewicz <norbert.jurkiewicz@gmail.com>
 */
class LoadStoragePlace extends AbstractFixture implements OrderedFixtureInterface {
    
    
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        //[name]
        $sampleData = [
            ['Storage place 1'],
            ['Storage place 2'],
            ['Storage place 3'],
            ['Storage place 4'],
            ['Storage place 5'],
        ];
        
        $i = 0;
        foreach($sampleData as $data){
            $storagePlace = new StoragePlace();
            $storagePlace->setName($data[0]);
            $this->addReference(get_class($storagePlace).DIRECTORY_SEPARATOR.$i++, $storagePlace);
            
            $manager->persist($storagePlace);
        }

        $manager->flush();
    }
    
    public function getOrder() {
        return 13;
    }
    
}
