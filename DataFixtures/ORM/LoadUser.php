<?php
namespace ApiBundle\ORM\DataFixtures;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use ApiBundle\Entity\UserRole;
use ApiBundle\Entity\User;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * 
 * Load user with exception of masters and licentiates
 *
 * @author Norbert Jurkiewicz <norbert.jurkiewicz@gmail.com>
 */
class LoadUser extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface {
    
    /**
     * @var ContainerInterface
     */
    private $container;
    
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager) {
        //[email, password, [role index], first_name, second_name, pesel, birth_date, worker_number, working_since_date, physician_tests_valid_date, position, address, is_consulting, consultation_date, bank_account_number, phone_number, room_number, lab]
        $sampleUsers = [
            ['user@just-code.it', 'user', [1], 'Username', 'Usersurname'],
            ['admin@just-code.it', 'admin', [2], 'Adminname', 'Adminurname'],
            ['owner@just-code.it', 'owner', [3], 'Ownername', 'Ownername'],
            ['person@just-code.it', 'person', [4], 'Personname', 'Personsurname'],
            ['person2@just-code.it', 'person2', [4], 'Personname2', 'Personsurname2'],
            ['person3@just-code.it', 'person3', [4], 'Personname3', 'Personsurname3'],
            ['person4@just-code.it', 'person4', [4], 'Personname4', 'Personsurname4'],
            ['person5@just-code.it', 'person5', [4], 'Personname5', 'Personsurname5'],
            ['worker@just-code.it', 'worker', [5], 'Workername', 'Workersurname', '77121511258', new \DateTime('1978-10-30'), 'A345X', new \DateTime('2012-03-31'), new \DateTime('2016-04-30'), 'junior cleaner', '12-345 Gdańsk Wyzwolenia 3A/4', true, new \DateTime('2015-06-30'), '1234512345456787', '123-456-789', 345, 'lab. info'],
            ['test@just-code.it', 'test', [8], 'Testname', 'Testsurname'],
            ['mateusz.barcikowski@biol.ug.edu.pl', 'mateusz123', [2], 'Mateusz', 'Barcikowski']
        ];
        
        $i = 0;
        foreach ($sampleUsers as $userData) {
            $user = new User();
            $user->setEmail($userData[0]);
            $encoder = $this->container->get('security.password_encoder');
            $user->setPassword($encoder->encodePassword($user, $userData[1]));
            foreach ($userData[2] as $roleIndex) {
                $user->addUserRole($this->getReference(UserRole::class . DIRECTORY_SEPARATOR . $roleIndex));
            }
            $user->setFirstName($userData[3]);
            $user->setLastName($userData[4]);
            $user->setIsActive(true);
            //add worker data
            if(in_array(5, $userData[2])){
                $user->setPesel($userData[5]);
                $user->setBirthDate($userData[6]);
                $user->setWorkerNumber($userData[7]);
                $user->setWorkingSinceDate($userData[8]);
                $user->setPhysicianTestsValidDate($userData[9]);
                $user->setPosition($userData[10]);
                $user->setAddress($userData[11]);
                $user->setIsConsulting($userData[12]);
                $user->setConsultationDate($userData[13]);
                $user->setBankAccountNumber($userData[14]);
                $user->setPhoneNumber($userData[15]);
                $user->setRoomNumber($userData[16]);
                $user->setLab($userData[17]);
            }
            $this->addReference(get_class($user) . DIRECTORY_SEPARATOR . $i++, $user);

            $manager->persist($user);
        }

        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
    
    public function getOrder() {
        return 3;
    }
    
}
