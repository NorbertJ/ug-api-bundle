<?php
namespace ApiBundle\ORM\DataFixtures;

use ApiBundle\Entity\Book;
use ApiBundle\Entity\User;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * 
 * Load sample books
 *
 * @author Norbert Jurkiewicz <norbert.jurkiewicz@gmail.com>
 */
class LoadBook extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface {
    
    /**
     * @var ContainerInterface
     */
    private $container;
    
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager) {
        //[[author -> userindex], year, title, publisher, isbn, bookstand, shelf, isBorrowed, owner_id, bar_code]
        $sampleData = [
            [[3,5],   2014, 'Test title 1', 'Ziomuś Gangsta', 'DS345-X' , '0789', 345, false],
            [[3,5,7], 2011, 'Test title 2', 'Słoń Trąbalski', 'DXZ-0345-X', '023', 56, true, 4, 1234],
            [[4], 1988, 'tytuł', 'Wydafca', 'DD-EELS', '1', '1', true, 5, 1234555],
            [[4], 1988, 'tytuł tom drugi', 'Wydafca', 'ssssss=x', '1', '1', true, 7, 1234234234]
        ];
        
        foreach ($sampleData as $data) {
            $book = new Book();
            foreach ($data[0] as $userIndex) {
                $book->addAuthor($this->getReference(User::class . DIRECTORY_SEPARATOR . $userIndex));
            }
            $book->setYear($data[1]);
            $book->setTitle($data[2]);
            $book->setPublisher($data[3]);
            $book->setIsbn($data[4]);
            $book->setBookstand($data[5]);
            $book->setShelf($data[6]);
            $book->setIsBorrowed($data[7]);
            if(isset($data[8]))
            {
                $book->setOwner($this->getReference(User::class . DIRECTORY_SEPARATOR . $data[8]));
            } else {
                $book->setOwner();
            }
            if(isset($data[9]))
            {
                $book->setBarCode($data[9]);
            }

            $manager->persist($book);
        }

        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
    
    public function getOrder() {
        return 6;
    }
    
}
