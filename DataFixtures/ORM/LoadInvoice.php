<?php
namespace ApiBundle\ORM\DataFixtures;

use ApiBundle\Entity\Company;
use ApiBundle\Entity\GrantPaymentSource;
use ApiBundle\Entity\Invoice;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Load client
 *
 * @author Norbert Jurkiewicz <norbert.jurkiewicz@gmail.com>
 */
class LoadInvoice extends AbstractFixture implements OrderedFixtureInterface {
    
    
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        //[company index, [payment source indexes], date, description, grossValue, material, category]
        $sampleData = [
            [1, [0,1], new \DateTime('2011-04-30'), 'test description 1', 123.45, 'test material 1', 'category 1', '2016/01/1234'],
            [2, [2], new \DateTime('2012-04-30'), 'test description 2', 223.45, 'test material 2', 'category 2', '2016/01/12345'],
            [3, [3], new \DateTime('2013-04-30'), 'test description 3', 323.45, 'test material 3', 'category 3', '2016/01/12346'],
            [4, [4,5], new \DateTime('2014-04-30'), 'test description 4', 423.45, 'test material 4', 'category 4', 'AR/FV/555'],
            [5, [6], new \DateTime('2014-05-30'), 'test description 5', 523.45, 'test material 5', 'category 2', 'ERT/555/f'],
        ];

        $i = 0;
        foreach ($sampleData as $data) {
            $invoice = new Invoice();
            $invoice->setCompany($this->getReference(Company::class . DIRECTORY_SEPARATOR . $data[0]));
            foreach ($data[1] as $paymentSourceIndex) {
                $paymentSource = $this->getReference(GrantPaymentSource::class . DIRECTORY_SEPARATOR . $paymentSourceIndex);
                $paymentSource->setInvoice($invoice);
            }
            $invoice->setDate($data[2]);
            $invoice->setDescription($data[3]);
            $invoice->setGrossValue($data[4]);
            $invoice->setMaterial($data[5]);
            $invoice->setCategory($data[6]);
            $invoice->setNumber($data[7]);

            $this->addReference(get_class($invoice) . DIRECTORY_SEPARATOR . $i++, $invoice);

            $manager->persist($invoice);
        }

        $manager->flush();
    }
    
    public function getOrder() {
        return 12;
    }
    
}
