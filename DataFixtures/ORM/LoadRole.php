<?php
namespace ApiBundle\ORM\DataFixtures;

use ApiBundle\Entity\UserRole;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Load user roles
 *
 * @author Norbert Jurkiewicz <norbert.jurkiewicz@gmail.com>
 */
class LoadRole extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface {
    
    /**
     * @var ContainerInterface
     */
    private $container;
    
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        //[name, role, client index or null]
        $sampleRoles = [
            ['role_guest', 'ROLE_GUEST'],
            ['role_user', 'ROLE_USER'],
            ['role_admin', 'ROLE_ADMIN'],
            ['role_owner', 'ROLE_OWNER'],
            ['role_person', 'ROLE_PERSON'],
            ['role_worker', 'ROLE_WORKER'],
            ['role_master', 'ROLE_MASTER'],
            ['role_licentiate', 'ROLE_LICENTIATE'],
            ['role_test', 'ROLE_TEST'],
        ];

        $i = 0;
        foreach($sampleRoles as $roleData){
            $role = new UserRole();
            $role->setName($roleData[0]);
            $role->setRole($roleData[1]);
            $this->addReference(get_class($role).DIRECTORY_SEPARATOR.$i++, $role);
            
            $manager->persist($role);
        }  

        $manager->flush();
    }
    
    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
    
    public function getOrder() {
        return 2;
    }
    
}
