<?php
namespace ApiBundle\ORM\DataFixtures;

use ApiBundle\Entity\Inventory;
use ApiBundle\Entity\Invoice;
use ApiBundle\Entity\StoragePlace;
use ApiBundle\Entity\User;
use DateTime;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Load client
 *
 * @author Norbert Jurkiewicz <norbert.jurkiewicz@gmail.com>
 */
class LoadInventory extends AbstractFixture implements OrderedFixtureInterface {
    
    
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        //[invoice index, value, type, serial_number, production_date, inventory_number, bar_code, use_place, responsible person index, storage place index, aparature_or_material]
        $sampleData = [
            [0, 234.45, 'type of item 1', 'AX-3456', new DateTime('2010-03-30'), '03456', '6906786554', 'use place 1', 3, 0, 'A'],
            [1, 334.45, 'type of item 2', 'AX-4456', new DateTime('2011-03-30'), '13456', '7906786554', 'use place 2', 4, 1, 'M'],
            [2, 434.45, 'type of item 3', 'AX-5456', new DateTime('2012-03-30'), '23456', '8906786554', 'use place 3', 5, 2, 'A'],
            [3, 534.45, 'type of item 4', 'AX-6456', new DateTime('2013-03-30'), '33456', '9906786554', 'use place 4', 6, 3, 'A'],
            [4, 634.45, 'type of item 5', 'AX-7456', new DateTime('2014-03-30'), 'S3456', '1906786554', 'use place 5', 7, 4, 'M'],
        ];
        
        $i = 0;
        foreach ($sampleData as $data) {
            $inventory = new Inventory();
            $inventory->setInvoice($this->getReference(Invoice::class . DIRECTORY_SEPARATOR . $data[0]));
            $inventory->setValue($data[1]);
            $inventory->setType($data[2]);
            $inventory->setSerialNumber($data[3]);
            $inventory->setProductionDate($data[4]);
            $inventory->setInventoryNumber($data[5]);
            $inventory->setBarCode($data[6]);
            $inventory->setUsePlace($data[7]);
            $inventory->setResponsiblePerson($this->getReference(User::class . DIRECTORY_SEPARATOR . $data[8]));
            $inventory->setStoragePlace($this->getReference(StoragePlace::class . DIRECTORY_SEPARATOR . $data[9]));
            $inventory->setAparatureOrMaterial($data[10]);

            $this->addReference(get_class($inventory) . DIRECTORY_SEPARATOR . $i++, $inventory);

            $manager->persist($inventory);
        }

        $manager->flush();
    }
    
    public function getOrder() {
        return 14;
    }
    
}
