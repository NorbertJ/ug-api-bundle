<?php
namespace ApiBundle\ORM\DataFixtures;

use ApiBundle\Entity\GrantPaymentSource;
use ApiBundle\Entity\User;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * 
 * Load sample grantPaymentSources
 *
 * @author Norbert Jurkiewicz <norbert.jurkiewicz@gmail.com>
 */
class LoadGrantPaymentSource extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface {
    
    /**
     * @var ContainerInterface
     */
    private $container;
    
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager) {
        //[manager - user index,[contractor -> userindex], city, country, description, fromDate, isPoster, name, title, toDate, year]
        $sampleData = [
            [3, [3,6], 'Test description 1', new \DateTime('2015-04-30'), true, 'Test name', 'XZ-345', 'Test source', 'GrantPaymentSource title 1',  new \DateTime('2015-02-30'), 1234.56, 1234.56],
            [4, [5,6], 'Test description 2', new \DateTime('2015-04-30'), true, 'Test name', 'XZ-345', 'Test source', 'GrantPaymentSource title 2',  new \DateTime('2015-02-30'), 987.65, 987.65],
            [5, [4,6,7], 'Test description 3', new \DateTime('2015-04-30'), true, 'Test name', 'XZ-345', 'Test source', 'GrantPaymentSource title 3',  new \DateTime('2015-02-30'), 666.66, 666.66],
            [5, [4,6,7], 'Test description 4', new \DateTime('2015-04-30'), true, 'Test name', 'XZ-345', 'Test source', 'GrantPaymentSource title 4',  new \DateTime('2015-02-30'), 666.66, 666.66],
            [5, [4,6,7], 'Test description 5', new \DateTime('2015-04-30'), true, 'Test name', 'XZ-345', 'Test source', 'GrantPaymentSource title 5',  new \DateTime('2015-02-30'), 666.66, 666.66],
            [5, [4,6,7], 'Test description 6', new \DateTime('2015-04-30'), true, 'Test name', 'XZ-345', 'Test source', 'GrantPaymentSource title 6',  new \DateTime('2015-02-30'), 666.66, 666.66],
            [6, [7], 'Test description 7', new \DateTime('2015-04-23'), true, 'Test name', 'XZ-345', 'Test source', 'GrantPaymentSource title 7',  new \DateTime('2015-02-30'), 777.77, 777.77],
        ];
        
        $i = 0;
        foreach ($sampleData as $data) {
            $grantPaymentSource = new GrantPaymentSource();
            $grantPaymentSource->setManager($this->getReference(User::class . DIRECTORY_SEPARATOR . $data[0]));
            foreach ($data[1] as $userIndex) {
                $grantPaymentSource->addContractor($this->getReference(User::class . DIRECTORY_SEPARATOR . $userIndex));
            }
            $grantPaymentSource->setDescription($data[2]);
            $grantPaymentSource->setFromDate($data[3]);
            $grantPaymentSource->setIsGrant($data[4]);
            $grantPaymentSource->setName($data[5]);
            $grantPaymentSource->setNumber($data[6]);
            $grantPaymentSource->setSource($data[7]);
            $grantPaymentSource->setTitle($data[8]);
            $grantPaymentSource->setToDate($data[9]);
            $grantPaymentSource->setTotalValue($data[10]);
            $grantPaymentSource->setCurrentValue($data[11]);
            $this->addReference(get_class($grantPaymentSource).DIRECTORY_SEPARATOR.$i++, $grantPaymentSource);

            $manager->persist($grantPaymentSource);
        }

        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
    
    public function getOrder() {
        return 10;
    }
    
}
