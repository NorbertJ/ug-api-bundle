<?php
namespace ApiBundle\ORM\DataFixtures;

use ApiBundle\Entity\Publication;
use ApiBundle\Entity\User;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * 
 * Load sample publications
 *
 * @author Norbert Jurkiewicz <norbert.jurkiewicz@gmail.com>
 */
class LoadPublication extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface {
    
    /**
     * @var ContainerInterface
     */
    private $container;
    
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager) {
        //[[author -> userindex], year, title, magazine, page_number, pdf, points]
        $sampleData = [
            [[3,6],   2014, 'Test title 1', 'Science', 345 , 'http://fwfewfewf/pdf', 45 ],
            [[5,6,7], 2011, 'Test title 2', 'New horizon', 35, 'http://ewfwwefewfew/pdf', 56],
        ];
        
        foreach ($sampleData as $data) {
            $publication = new Publication();
            foreach ($data[0] as $userIndex) {
                $publication->addAuthor($this->getReference(User::class . DIRECTORY_SEPARATOR . $userIndex));
            }
            $publication->setYear($data[1]);
            $publication->setTitle($data[2]);
            $publication->setMagazine($data[3]);
            $publication->setPageNumber($data[4]);
            $publication->setPdf($data[5]);
            $publication->setPoints($data[6]);

            $manager->persist($publication);
        }

        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
    
    public function getOrder() {
        return 7;
    }
    
}
