<?php
namespace ApiBundle\ORM\DataFixtures;

use ApiBundle\Entity\Client;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use OAuth2\OAuth2;

/**
 * Load client
 *
 * @author Norbert Jurkiewicz <norbert.jurkiewicz@gmail.com>
 */
class LoadClient extends AbstractFixture implements OrderedFixtureInterface {
    
    
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $commonRedirects = [
            'http://ug-admin.dev.kansi.eu/',
            'http://ug-admin.show.kansi.eu/',
            'http://ug-admin.local/',
            'http://localhost:9001/',
            'http://localhost/',
            'http://ug.just-code.it',
            'http://ug-biblioteka.just-code.it'
        ];
        //[randomId, [redirectUri], secret, [allowedGrantTypes]]
        $sampleClients = [
            ['kansi', $commonRedirects, 'kansi', [OAuth2::GRANT_TYPE_REFRESH_TOKEN ,OAuth2::GRANT_TYPE_AUTH_CODE, OAuth2::GRANT_TYPE_CLIENT_CREDENTIALS, OAuth2::GRANT_TYPE_IMPLICIT, OAuth2::GRANT_TYPE_USER_CREDENTIALS]],
            ['test', $commonRedirects, 'test', [OAuth2::GRANT_TYPE_REFRESH_TOKEN ,OAuth2::GRANT_TYPE_AUTH_CODE, OAuth2::GRANT_TYPE_CLIENT_CREDENTIALS, OAuth2::GRANT_TYPE_IMPLICIT, OAuth2::GRANT_TYPE_USER_CREDENTIALS]],
        ];
        
        $i = 0;
        foreach($sampleClients as $clientData){
            $client = new Client();
            $client->setRandomId($clientData[0]);
            $client->setRedirectUris($clientData[1]);
            $client->setSecret($clientData[2]);
            $client->setAllowedGrantTypes($clientData[3]);
            
            $this->addReference('client_'.$i++, $client);
            
            $manager->persist($client);
        }

        $manager->flush();
    }
    
    public function getOrder() {
        return 5;
    }
    
}
