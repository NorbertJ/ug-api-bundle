<?php
namespace ApiBundle\ORM\DataFixtures;

use ApiBundle\Entity\Conference;
use ApiBundle\Entity\User;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * 
 * Load sample conferences
 *
 * @author Norbert Jurkiewicz <norbert.jurkiewicz@gmail.com>
 */
class LoadConference extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface {
    
    /**
     * @var ContainerInterface
     */
    private $container;
    
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager) {
        //[[author -> userindex], city, country, description, fromDate, isPoster, name, title, toDate, year]
        $sampleData = [
            [[3,6], 'New York', 'USA', 'Talking about nothing', new \DateTime('2015-04-30'), true, 'Conference name 1', 'Conference title 1',  new \DateTime('2015-02-30'), 2014],
            [[3,5,7], 'Tokyo', 'Japan', 'Talking about nothing', new \DateTime('2015-04-30'), true, 'Conference name 2', 'Conference title 2',  new \DateTime('2015-04-30'), 2013],
            [[5], 'Havana', 'Cuba', 'Talking about nothing', new \DateTime('2015-04-30'), true, 'Conference name 3', 'Conference title 3',  new \DateTime('2015-05-30'), 2015],
        ];
        
        foreach ($sampleData as $data) {
            $conference = new Conference();
            foreach ($data[0] as $userIndex) {
                $conference->addAuthor($this->getReference(User::class . DIRECTORY_SEPARATOR . $userIndex));
            }
            $conference->setCity($data[1]);
            $conference->setCountry($data[2]);
            $conference->setDescription($data[3]);
            $conference->setFromDate($data[4]);
            $conference->setIsPoster($data[5]);
            $conference->setName($data[6]);
            $conference->setTitle($data[7]);
            $conference->setToDate($data[8]);
            $conference->setYear($data[9]);

            $manager->persist($conference);
        }

        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
    
    public function getOrder() {
        return 9;
    }
    
}
