<?php
namespace ApiBundle\ORM\DataFixtures;

use ApiBundle\Entity\StudiesType;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * 
 * Load studies type
 *
 * @author Norbert Jurkiewicz <norbert.jurkiewicz@gmail.com>
 */
class LoadStudiesType extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface {
    
    /**
     * @var ContainerInterface
     */
    private $container;
    
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager) {
        //name
        $sampleData = [
            ['stacjonarne'],
            ['zaoczne'],
        ];

        $i = 0;
        foreach ($sampleData as $data) {
            $type = new StudiesType();
            $type->setName($data[0]);
            $this->addReference(get_class($type) . DIRECTORY_SEPARATOR . $i++, $type);

            $manager->persist($type);
        }

        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
    
    public function getOrder() {
        return 1;
    }
    
}
