<?php
namespace ApiBundle\ORM\DataFixtures;

use ApiBundle\Entity\Thesis;
use ApiBundle\Entity\User;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * 
 * Load sample thesis
 *
 * @author Norbert Jurkiewicz <norbert.jurkiewicz@gmail.com>
 */
class LoadThesis extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface {
    
    /**
     * @var ContainerInterface
     */
    private $container;
    
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager) {
        //return;
        //[[author -> userindex], year, title, [promotor -> user index], specie, place, group, bookstand, shelf, degree]
        $sampleData = [
            [[3,6],2014,'Test title 1',[5,7],'Chemistry','test place','test group','test bookstand','test shelf','test degree'],
            [[5,6,7],2011,'Test title 2',[3,6],'Biology','test place','test group','test bookstand','test shelf','test degree'],
        ];
        
        foreach ($sampleData as $data) {
            $thesis = new Thesis();
            foreach ($data[0] as $userIndex) {
                $thesis->addAuthor($this->getReference(User::class . DIRECTORY_SEPARATOR . $userIndex));
            }
            $thesis->setYear($data[1]);
            $thesis->setTitle($data[2]);
            foreach ($data[3] as $userIndex) {
                $thesis->addPromotor($this->getReference(User::class . DIRECTORY_SEPARATOR . $userIndex));
            }
            $thesis->setSpecie($data[4]);
            $thesis->setPlace($data[5]);
            $thesis->setGroup($data[6]);
            $thesis->setBookstand($data[7]);
            $thesis->setShelf($data[8]);
            $thesis->setDegree($data[9]);

            $manager->persist($thesis);
        }

        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
    
    public function getOrder() {
        return 8;
    }
    
}
