<?php
namespace ApiBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="ApiBundle\Repository\ThesisRepository")
 * @ORM\Table(name="thesis")
 * @author Norbert Jurkiewicz <norbert.jurkiewicz@gmail.com>
 */
class Thesis
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToMany(targetEntity="User", inversedBy="thesis", cascade={"persist"})
     * @ORM\JoinTable(name="user_has_thesis")
     */
    protected $authors;
    
    /**
     * @ORM\ManyToMany(targetEntity="User", inversedBy="thesisPromotors", cascade={"persist"})
     * @ORM\JoinTable(name="user_has_thesis_promotor")
     */
    protected $promotors;
    
    /**
     * @ORM\Column(type="string")
     */
    protected $year;
    
    /**
     * @ORM\Column(type="string")
     */
    protected $title;
    
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $specie;
    
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $place;
    
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $bookstand;
    
    /**
     * @ORM\Column(name="`group`", type="string", nullable=true)
     */
    protected $group;
    
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $shelf;
    
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $degree;
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->authors = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set year
     *
     * @param string $year
     *
     * @return Thesis
     */
    public function setYear($year)
    {
        $this->year = $year;

        return $this;
    }

    /**
     * Get year
     *
     * @return string
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Thesis
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set specie
     *
     * @param string $specie
     *
     * @return Thesis
     */
    public function setSpecie($specie)
    {
        $this->specie = $specie;

        return $this;
    }

    /**
     * Get specie
     *
     * @return string
     */
    public function getSpecie()
    {
        return $this->specie;
    }

    /**
     * Set place
     *
     * @param string $place
     *
     * @return Thesis
     */
    public function setPlace($place)
    {
        $this->place = $place;

        return $this;
    }

    /**
     * Get place
     *
     * @return string
     */
    public function getPlace()
    {
        return $this->place;
    }

    /**
     * Set bookstand
     *
     * @param string $bookstand
     *
     * @return Thesis
     */
    public function setBookstand($bookstand)
    {
        $this->bookstand = $bookstand;

        return $this;
    }

    /**
     * Get bookstand
     *
     * @return string
     */
    public function getBookstand()
    {
        return $this->bookstand;
    }

    /**
     * Set group
     *
     * @param string $group
     *
     * @return Thesis
     */
    public function setGroup($group)
    {
        $this->group = $group;

        return $this;
    }

    /**
     * Get group
     *
     * @return string
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * Set shelf
     *
     * @param string $shelf
     *
     * @return Thesis
     */
    public function setShelf($shelf)
    {
        $this->shelf = $shelf;

        return $this;
    }

    /**
     * Get shelf
     *
     * @return string
     */
    public function getShelf()
    {
        return $this->shelf;
    }

    /**
     * Set degree
     *
     * @param string $degree
     *
     * @return Thesis
     */
    public function setDegree($degree)
    {
        $this->degree = $degree;

        return $this;
    }

    /**
     * Get degree
     *
     * @return string
     */
    public function getDegree()
    {
        return $this->degree;
    }

    /**
     * Add author
     *
     * @param User $author
     *
     * @return Thesis
     */
    public function addAuthor(User $author)
    {
        $this->authors[] = $author;

        return $this;
    }

    /**
     * Remove author
     *
     * @param User $author
     */
    public function removeAuthor(User $author)
    {
        $this->authors->removeElement($author);
    }

    /**
     * Get authors
     *
     * @return Collection
     */
    public function getAuthors()
    {
        return $this->authors;
    }

    /**
     * Add promotor
     *
     * @param User $promotor
     *
     * @return Thesis
     */
    public function addPromotor(User $promotor)
    {
        $this->promotors[] = $promotor;

        return $this;
    }

    /**
     * Remove promotor
     *
     * @param User $promotor
     */
    public function removePromotor(User $promotor)
    {
        $this->promotors->removeElement($promotor);
    }

    /**
     * Get promotors
     *
     * @return Collection
     */
    public function getPromotors()
    {
        return $this->promotors;
    }
}
