<?php
namespace ApiBundle\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * Inventory
 * 
 * @ORM\Entity(repositoryClass="ApiBundle\Repository\InventoryRepository")
 * @ORM\Table(name="inventory")
 * @author Norbert Jurkiewicz <norbert.jurkiewicz@gmail.com>
 */
class Inventory
{
    
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
  
    /**
     * @var Invoice
     * 
     * @ORM\ManyToOne(targetEntity="Invoice", inversedBy="inventories")
     * @ORM\JoinColumn(name="invoice_id", referencedColumnName="id", nullable=true)
     */
    protected $invoice;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $isArchived;

    /**
     * @ORM\Column(type="string")
     */
    protected $value;
    
    /**
     * @ORM\Column(type="string")
     */
    protected $type;
    
    /**
     * @ORM\Column(type="string")
     */
    protected $serialNumber;
    
    /**
     * @var DateTime
     *
     * @ORM\Column(type="date", nullable=true)
     */
    protected $productionDate;
    
    /**
     * @ORM\Column(type="string")
     */
    protected $inventoryNumber;
    
    /**
     * @ORM\Column(type="string")
     */
    protected $barCode;
    
    /**
     * @ORM\Column(type="string")
     */
    protected $usePlace;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="inventory")
     * @ORM\JoinColumn(name="owner_id", referencedColumnName="id")
     */

    protected $owner;

    /**
     * @return mixed
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * @param mixed $owner
     * @return  Inventory
     */
    public function setOwner(User $owner = null)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsArchived()
    {
        return $this->isArchived;
    }

    /**
     * @param mixed $isArchived
     * @return Inventory
     */
    public function setIsArchived($isArchived)
    {
        $this->isArchived = $isArchived;

        return $this;
    }

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */

    protected $isBorrowed;

    /**
     * @return mixed
     */
    public function getIsBorrowed()
    {
        return $this->isBorrowed;
    }

    /**
     * @param mixed $isBorrowed
     * @return  Inventory
     */
    public function setIsBorrowed($isBorrowed)
    {
        $this->isBorrowed = $isBorrowed;
        return $this;
    }

    /**
     * @var DateTime
     *
     * @ORM\Column(type="date", nullable=true)
     */
    private $returnDate;

    /**
     * @return mixed
     */
    public function getReturnDate()
    {
        return $this->returnDate;
    }

    /**
     * @param mixed $returnDate
     * @return Inventory
     */
    public function setReturnDate($returnDate)
    {
        $this->returnDate = $returnDate;
        return $this;
    }

    /**
     * @var User
     * 
     * @ORM\ManyToOne(targetEntity="User", inversedBy="inventories")
     * @ORM\JoinColumn(name="responsible_person_id", referencedColumnName="id")
     */
    protected $responsiblePerson;
    
    /**
     * @var StoragePlace
     * 
     * @ORM\ManyToOne(targetEntity="StoragePlace", inversedBy="inventories")
     * @ORM\JoinColumn(name="storage_place_id", referencedColumnName="id")
     */
    protected $storagePlace;
    
    /**
     * @ORM\Column(type="string")
     */
    protected $aparatureOrMaterial;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value
     *
     * @param string $value
     *
     * @return Inventory
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Inventory
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set serialNumber
     *
     * @param string $serialNumber
     *
     * @return Inventory
     */
    public function setSerialNumber($serialNumber)
    {
        $this->serialNumber = $serialNumber;

        return $this;
    }

    /**
     * Get serialNumber
     *
     * @return string
     */
    public function getSerialNumber()
    {
        return $this->serialNumber;
    }

    /**
     * Set productionDate
     *
     * @param \DateTime $productionDate
     *
     * @return Inventory
     */
    public function setProductionDate($productionDate)
    {
        $this->productionDate = $productionDate;

        return $this;
    }

    /**
     * Get productionDate
     *
     * @return \DateTime
     */
    public function getProductionDate()
    {
        return $this->productionDate;
    }

    /**
     * Set inventoryNumber
     *
     * @param string $inventoryNumber
     *
     * @return Inventory
     */
    public function setInventoryNumber($inventoryNumber)
    {
        $this->inventoryNumber = $inventoryNumber;

        return $this;
    }

    /**
     * Get inventoryNumber
     *
     * @return string
     */
    public function getInventoryNumber()
    {
        return $this->inventoryNumber;
    }

    /**
     * Set barCode
     *
     * @param string $barCode
     *
     * @return Inventory
     */
    public function setBarCode($barCode)
    {
        $this->barCode = $barCode;

        return $this;
    }

    /**
     * Get barCode
     *
     * @return string
     */
    public function getBarCode()
    {
        return $this->barCode;
    }

    /**
     * Set usePlace
     *
     * @param string $usePlace
     *
     * @return Inventory
     */
    public function setUsePlace($usePlace)
    {
        $this->usePlace = $usePlace;

        return $this;
    }

    /**
     * Get usePlace
     *
     * @return string
     */
    public function getUsePlace()
    {
        return $this->usePlace;
    }

    /**
     * Set aparatureOrMaterial
     *
     * @param string $aparatureOrMaterial
     *
     * @return Inventory
     */
    public function setAparatureOrMaterial($aparatureOrMaterial)
    {
        $this->aparatureOrMaterial = $aparatureOrMaterial;

        return $this;
    }

    /**
     * Get aparatureOrMaterial
     *
     * @return string
     */
    public function getAparatureOrMaterial()
    {
        return $this->aparatureOrMaterial;
    }

    /**
     * Set invoice
     *
     * @param Invoice $invoice
     *
     * @return Inventory
     */
    public function setInvoice(Invoice $invoice = null)
    {
        $this->invoice = $invoice;

        return $this;
    }

    /**
     * Get invoice
     *
     * @return Invoice
     */
    public function getInvoice()
    {
        return $this->invoice;
    }

    /**
     * Set responsiblePerson
     *
     * @param User $responsiblePerson
     *
     * @return Inventory
     */
    public function setResponsiblePerson(User $responsiblePerson = null)
    {
        $this->responsiblePerson = $responsiblePerson;

        return $this;
    }

    /**
     * Get responsiblePerson
     *
     * @return User
     */
    public function getResponsiblePerson()
    {
        return $this->responsiblePerson;
    }

    /**
     * Set storagePlace
     *
     * @param StoragePlace $storagePlace
     *
     * @return Inventory
     */
    public function setStoragePlace(StoragePlace $storagePlace = null)
    {
        $this->storagePlace = $storagePlace;

        return $this;
    }

    /**
     * Get storagePlace
     *
     * @return StoragePlace
     */
    public function getStoragePlace()
    {
        return $this->storagePlace;
    }
}
