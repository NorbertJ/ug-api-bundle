<?php
namespace ApiBundle\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Invoice
 * 
 * @ORM\Table(name="invoice")
 * @ORM\Entity(repositoryClass="ApiBundle\Repository\InvoiceRepository")
 * 
 * @author Norbert Jurkiewicz <norbert.jurkiewicz@gmail.com>
 */
class Invoice {

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    protected $number;


    /**
     * @ORM\ManyToOne(targetEntity="Company")
     * @ORM\JoinColumn(name="company_id", referencedColumnName="id")
     */
    protected $company;

    /**
     * @ORM\ManyToOne(targetEntity="IncomeOutcome")
     * @ORM\JoinColumn(name="incomeoutcome_id", referencedColumnName="id")
     */
    protected $incomeoutcome;

    /**
     * @return mixed
     */
    public function getIncomeoutcome()
    {
        return $this->incomeoutcome;
    }

    /**
     * @param $incomeoutcome
     * @return Invoice
     */
    public function setIncomeoutcome($incomeoutcome)
    {
        $this->incomeoutcome = $incomeoutcome;

        return $this;
    }


    
    /**
     * @ORM\OneToMany(targetEntity="PaymentSource", mappedBy="invoice", cascade={"persist"})
     */
    protected $paymentSources;
    
    /**
     * @var DateTime
     *
     * @ORM\Column(type="date")
     */
    private $date;
    
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $description;
    
    /**
     * @var float
     *
     * @ORM\Column(type="decimal", precision=10, scale=2)
     * @Assert\Type(
     *     type="float"
     * )
     * @Assert\Range(
     *      min = 0,
     *      max = 99999999999999
     * )
     * @ORM\Column(type="float")
     */
    private $grossValue;
    
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $material;
    
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $category;
    
    /**
     * @ORM\OneToMany(targetEntity="Inventory", mappedBy="invoice")
     */
    private $inventories;
    
    public function __construct() {
        $this->paymentSources = new ArrayCollection();
        $this->inventories = new ArrayCollection();
    }


    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Invoice
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @return mixed
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     *
     * @param mixed $number
     * @return Invoice
     */

    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }
    /**
     * Set description
     *
     * @param string $description
     *
     * @return Invoice
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set grossValue
     *
     * @param string $grossValue
     *
     * @return Invoice
     */
    public function setGrossValue($grossValue)
    {
        $this->grossValue = $grossValue;

        return $this;
    }

    /**
     * Get grossValue
     *
     * @return string
     */
    public function getGrossValue()
    {
        return $this->grossValue;
    }

    /**
     * Set material
     *
     * @param string $material
     *
     * @return Invoice
     */
    public function setMaterial($material)
    {
        $this->material = $material;

        return $this;
    }

    /**
     * Get material
     *
     * @return string
     */
    public function getMaterial()
    {
        return $this->material;
    }

    /**
     * Set category
     *
     * @param string $category
     *
     * @return Invoice
     */
    public function setCategory($category)
    {
        $this->category = $category;

        return $this;
    }


    /**
     * Get category
     *
     * @return string
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set company
     *
     * @param Company $company
     *
     * @return Invoice
     */
    public function setCompany(Company $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Add paymentSource
     *
     * @param PaymentSource $paymentSource
     *
     * @return Invoice
     */
    public function addPaymentSource(PaymentSource $paymentSource)
    {
        $this->paymentSources[] = $paymentSource;

        return $this;
    }

    /**
     * Remove paymentSource
     *
     * @param PaymentSource $paymentSource
     */
    public function removePaymentSource(PaymentSource $paymentSource)
    {
        $this->paymentSources->removeElement($paymentSource);
    }

    /**
     * Get paymentSources
     *
     * @return Collection
     */
    public function getPaymentSources()
    {
        return $this->paymentSources;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add inventory
     *
     * @param Inventory $inventory
     *
     * @return Invoice
     */
    public function addInventory(Inventory $inventory)
    {
        $this->inventories[] = $inventory;

        return $this;
    }

    /**
     * Remove inventory
     *
     * @param Inventory $inventory
     */
    public function removeInventory(Inventory $inventory)
    {
        $this->inventories->removeElement($inventory);
    }

    /**
     * Get inventories
     *
     * @return Collection
     */
    public function getInventories()
    {
        return $this->inventories;
    }
}
