<?php
/**
 * File Description
 *
 * Some comprehensive Description of the files contents
 *
 * @author Norbert Jurkiewicz <norbert.jurkiewicz@interactivedata.com>
 * @user norbert
 * @version $Author:$ $Id:$ $Revision:$ $Date:$
 * @since 26.09.2016
 */

namespace ApiBundle\Entity;

use ApiBundle\Repository\InvoiceRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity (repositoryClass="ApiBundle\Repository\IncomeOutcomeRepository")
 * @Orm\Table(name="incomeoutcome")
 *
 */
class IncomeOutcome
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     */
    protected $description;


    /**
     * @return
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @var float
     *
     * @ORM\Column(type="decimal", precision=10, scale=2)
     * @Assert\Type(
     *     type="float"
     * )
     * @Assert\Range(
     *      min = -10000000,
     *      max = 100000000
     * )
     * @ORM\Column(type="float")
     */
    protected $value;

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     * @return IncomeOutcome
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }

    /**
     * @ORM\ManyToOne(targetEntity="Invoice", inversedBy="incomeoutcome", cascade={"persist"})
     * @ORM\JoinColumn(name="invoice_id", referencedColumnName="id", nullable=true)
     */
    private $invoice;

    /**
     * @return Invoice
     *
     *
     */
    public function getInvoice()
    {
        return $this->invoice;
    }

    /**
     * @param $invoice
     * @return IncomeOutcome
     */
    public function setInvoice($invoice)
    {
        $this->invoice = $invoice;
        return $this;
    }


    /**
     * @ORM\ManyToOne(targetEntity="GrantPaymentSource", inversedBy="incomeoutcome", cascade={"persist"})
     * @ORM\JoinColumn(name="grantpaymentsource_id", referencedColumnName="id", nullable=true)
     */
    private $grantpaymentsource;

    /**
     * @return GrantPaymentSource
     *
     */
    public function getGrantpaymentsource()
    {
        return $this->grantpaymentsource;
    }

    /**
     * @param mixed $grantpaymentsource
     * @return IncomeOutcome
     */
    public function setGrantpaymentsource($grantpaymentsource)
    {
        $this->grantpaymentsource = $grantpaymentsource;
        return $this;
    }


    /**
     * @param mixed $id
     * @return IncomeOutcome
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     * @return  IncomeOutcome
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }


}