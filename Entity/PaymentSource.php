<?php
namespace ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * PaymentSource
 * 
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * @ORM\DiscriminatorMap({
 *      "grant" = "GrantPaymentSource",
 * })
 * @ORM\Table(name="payment_source")
 * @ORM\Entity(repositoryClass="ApiBundle\Repository\PaymentSourceRepository")
 * 
 * @author Norbert Jurkiewicz <norbert.jurkiewicz@gmail.com>
 */
abstract class PaymentSource {

    /**
     * Allowed payment sources
     */
    const TYPE_GRANT = 'grant';

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     */
    protected $name;
    
    /**
     * @ORM\ManyToOne(targetEntity="Invoice", inversedBy="paymentSources", cascade={"persist"})
     * @ORM\JoinColumn(name="invoice_id", referencedColumnName="id")
     */
    private $invoice;
    
    /**
     * @var float
     *
     * @ORM\Column(type="decimal", precision=10, scale=2)
     * @Assert\Type(
     *     type="float"
     * )
     * @Assert\Range(
     *      min = 0,
     *      max = 100000000
     * )
     * @ORM\Column(type="float")
     */
    protected $totalValue;

    /**
     * @var float
     *
     * @ORM\Column(type="decimal", precision=10, scale=2)
     * @Assert\Type(
     *     type="float"
     * )
     * @Assert\Range(
     *      min = 0,
     *      max = 100000000
     * )
     * @ORM\Column(type="float")
     */
    protected $currentValue;
    
    /**
     * @ORM\Column(type="boolean")
     */
    protected $isGrant = false;
    
    /**
     * Get all allowed payment source types
     * @return array
     */
    public function getAllTypes(){

        return [
            self::TYPE_GRANT,
        ];
    }
    

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return PaymentSource
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set totalValue
     *
     * @param string $totalValue
     *
     * @return PaymentSource
     */
    public function setTotalValue($totalValue)
    {
        $this->totalValue = $totalValue;

        return $this;
    }

    /**
     * Get totalValue
     *
     * @return string
     */
    public function getTotalValue()
    {
        return $this->totalValue;
    }

    /**
     * Set currentValue
     *
     * @param string $totalValue
     *
     * @return PaymentSource
     */
    public function setCurrentValue($totalValue)
    {
        $this->currentValue = $totalValue;

        return $this;
    }

    /**
     * Get currentValue
     *
     * @return string
     */
    public function getCurrentValue()
    {
        return $this->currentValue;
    }

    /**
     * Set isGrant
     *
     * @param boolean $isGrant
     *
     * @return PaymentSource
     */
    public function setIsGrant($isGrant)
    {
        $this->isGrant = $isGrant;

        return $this;
    }

    /**
     * Get isGrant
     *
     * @return boolean
     */
    public function getIsGrant()
    {
        return $this->isGrant;
    }

    /**
     * Set invoice
     *
     * @param Invoice $invoice
     *
     * @return PaymentSource
     */
    public function setInvoice(Invoice $invoice = null)
    {
        $this->invoice = $invoice;

        return $this;
    }

    /**
     * Get invoice
     *
     * @return Invoice
     */
    public function getInvoice()
    {
        return $this->invoice;
    }
}
