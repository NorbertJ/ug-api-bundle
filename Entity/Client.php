<?php
namespace ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\OAuthServerBundle\Entity\Client as BaseClient;

/**
 * Clients
 * 
 * @ORM\Entity(repositoryClass="ApiBundle\Repository\ClientRepository")
 * @ORM\Table(name="client")
 * @author Norbert Jurkiewicz <norbert.jurkiewicz@gmail.com>
 */
class Client extends BaseClient
{
    
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

}
