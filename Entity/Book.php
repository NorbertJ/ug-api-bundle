<?php
namespace ApiBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use DateTime;

/**
 * @ORM\Entity(repositoryClass="ApiBundle\Repository\BookRepository")
 * @ORM\Table(name="book")
 * @author Norbert Jurkiewicz <norbert.jurkiewicz@gmail.com>
 */
class Book
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToMany(targetEntity="User", inversedBy="books", cascade={"persist"})
     * @ORM\JoinTable(name="user_has_book")
     */
    protected $authors;

    /**
     * @ORM\Column(type="string")
     */
    protected $year;
    
    /**
     * @ORM\Column(type="string")
     */
    protected $title;
    
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $publisher;
    
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $isbn;
    
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $bookstand;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="books")
     * @ORM\JoinColumn(name="owner_id", referencedColumnName="id")
     */

    protected $owner;

    /**
     * @return mixed
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * @param mixed $owner
     * @return  Book
     */
    public function setOwner(User $owner = null)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */

    protected $isBorrowed;

    /**
     * @return mixed
     */
    public function getIsBorrowed()
    {
        return $this->isBorrowed;
    }

    /**
     * @param mixed $isBorrowed
     * @return  Book
     */
    public function setIsBorrowed($isBorrowed)
    {
        $this->isBorrowed = $isBorrowed;
        return $this;
    }

    /**
     * @var DateTime
     *
     * @ORM\Column(type="date", nullable=true)
     */
    private $returnDate;

    /**
     * @return mixed
     */
    public function getReturnDate()
    {
        return $this->returnDate;
    }

    /**
     * @param mixed $returnDate
     * @return Book
     */
    public function setReturnDate($returnDate)
    {
        $this->returnDate = $returnDate;
        return $this;
    }

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $barCode;

    /**
     * @return mixed
     */
    public function getBarCode()
    {
        return $this->barCode;
    }

    /**
     * @param mixed $barCode
     * @return Book
     */
    public function setBarCode($barCode)
    {
        $this->barCode = $barCode;
        return $this;
    }




    /**
     * @ORM\Column(type="string", nullable=true)
     */

    protected $shelf;
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->authors = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set year
     *
     * @param string $year
     *
     * @return Book
     */
    public function setYear($year)
    {
        $this->year = $year;

        return $this;
    }

    /**
     * Get year
     *
     * @return string
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Book
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set publisher
     *
     * @param string $publisher
     *
     * @return Book
     */
    public function setPublisher($publisher)
    {
        $this->publisher = $publisher;

        return $this;
    }

    /**
     * Get publisher
     *
     * @return string
     */
    public function getPublisher()
    {
        return $this->publisher;
    }

    /**
     * Set isbn
     *
     * @param string $isbn
     *
     * @return Book
     */
    public function setIsbn($isbn)
    {
        $this->isbn = $isbn;

        return $this;
    }

    /**
     * Get isbn
     *
     * @return string
     */
    public function getIsbn()
    {
        return $this->isbn;
    }

    /**
     * Set bookstand
     *
     * @param string $bookstand
     *
     * @return Book
     */
    public function setBookstand($bookstand)
    {
        $this->bookstand = $bookstand;

        return $this;
    }

    /**
     * Get bookstand
     *
     * @return string
     */
    public function getBookstand()
    {
        return $this->bookstand;
    }

    /**
     * Set shelf
     *
     * @param string $shelf
     *
     * @return Book
     */
    public function setShelf($shelf)
    {
        $this->shelf = $shelf;

        return $this;
    }

    /**
     * Get shelf
     *
     * @return string
     */
    public function getShelf()
    {
        return $this->shelf;
    }

    /**
     * Add author
     *
     * @param User $author
     *
     * @return Book
     */
    public function addAuthor(User $author)
    {
        $this->authors[] = $author;

        return $this;
    }

    /**
     * Remove author
     *
     * @param User $author
     */
    public function removeAuthor(User $author)
    {
        $this->authors->removeElement($author);
    }

    /**
     * Get authors
     *
     * @return Collection
     */
    public function getAuthors()
    {
        return $this->authors;
    }
}
