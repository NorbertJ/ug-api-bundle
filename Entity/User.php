<?php

namespace ApiBundle\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Serializable;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\EquatableInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * User
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="ApiBundle\Repository\UserRepository")
 * @UniqueEntity("email")
 */
class User implements UserInterface, Serializable, EquatableInterface
{
    
    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=100, nullable=false)
     */
    private $password;

    /**
     * @Assert\Length(max = 4096)
     */
    protected $plainPassword;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=45, nullable=false, unique=true)
     * @Assert\Email(
     *     message = "The email '{{ value }}' is not a valid email.",
     *     checkMX = true
     * )
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=32)
     */
    protected $salt;
    
    /**
     * @ORM\ManyToMany(targetEntity="UserRole", inversedBy="users", cascade={"persist"})
     * @ORM\JoinTable(name="user_has_role")
     */
    protected $userRoles;

    /**
     * @ORM\OneToMany(targetEntity="AccessToken", mappedBy="user")
     * @ORM\OrderBy({"expiresAt" = "DESC"})
     * */
    protected $accessTokens;

    /**
     * @ORM\OneToMany(targetEntity="AuthCode", mappedBy="user")
     * @ORM\OrderBy({"expiresAt" = "DESC"})
     * */
    protected $authCodes;
    
    /**
     * @ORM\Column(type="boolean")
     */
    protected $isActive = false;
    
    /**
     * @ORM\Column(type="string", length=50)
     */
    protected $firstName;
    
    /**
     * @ORM\Column(type="string", length=99)
     */
    protected $lastName;
    
    /**
     * @ORM\Column(type="string", length=11, nullable=true)
     */
    protected $pesel;
    
    /**
     * @var DateTime
     *
     * @ORM\Column(type="date", nullable=true)
     */
    private $birthDate;
    
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $workerNumber;
    
    /**
     * @var DateTime
     *
     * @ORM\Column(type="date", nullable=true)
     */
    private $workingSinceDate;
    
    /**
     * @var DateTime
     *
     * @ORM\Column(type="date", nullable=true)
     */
    private $physicianTestsValidDate;
    
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $position;
    
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $address;
    
    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $isConsulting;
    
    /**
     * @var DateTime
     *
     * @ORM\Column(type="date", nullable=true)
     */
    private $consultationDate;
    
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $bankAccountNumber;
    
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $phoneNumber;
    
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $roomNumber;
    
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $lab;
    
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $title;
    
    /**
     * @ORM\ManyToMany(targetEntity="User", mappedBy="promotors")
     */
    protected $promotedUsers;
    
    /**
     * @ORM\ManyToMany(targetEntity="User", inversedBy="promotedUsers", cascade={"persist"})
     * @ORM\JoinTable(name="user_has_promotor",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="promotor_user_id", referencedColumnName="id")}
     *      )
     */
    protected $promotors;
    
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $fieldOfStudies;
    
    /**
     * @ORM\ManyToOne(targetEntity="StudiesType")
     * @ORM\JoinColumn(name="studies_type_id", referencedColumnName="id")
     */
    protected $typeOfStudies;
    
    /**
     * @ORM\ManyToMany(targetEntity="Book", mappedBy="authors")
     */
    private $books;
    
    /**
     * @ORM\ManyToMany(targetEntity="Publication", mappedBy="authors")
     */
    private $publications;
    
    /**
     * @ORM\ManyToMany(targetEntity="Conference", mappedBy="authors")
     */
    private $conferences;
    
    /**
     * @ORM\ManyToMany(targetEntity="Thesis", mappedBy="authors")
     */
    private $thesis;
    
    /**
     * @ORM\ManyToMany(targetEntity="Thesis", mappedBy="promotors")
     */
    private $thesisPromotors;
    
    /**
     * @ORM\ManyToMany(targetEntity="GrantPaymentSource", mappedBy="contractors")
     */
    private $grantPaymentSourceContractors;
    
    /**
     * @ORM\OneToMany(targetEntity="Inventory", mappedBy="responsiblePerson")
     */
    private $inventories;
    
    public function __construct() {
        $this->salt = md5(uniqid(null, true));
        $this->userRoles = new ArrayCollection();
        $this->accessTokens = new ArrayCollection();
        $this->authCodes = new ArrayCollection();
        $this->books = new ArrayCollection();
        $this->publications = new ArrayCollection();
        $this->thesis = new ArrayCollection();
        $this->thesisPromotors = new ArrayCollection();
        $this->conferences = new ArrayCollection();
        $this->grantPaymentSourceContractors = new ArrayCollection();
        $this->inventories = new ArrayCollection();
    }

    /**
     * @inheritDoc
     */
    public function getUsername() {
        
        return $this->email;
    }
    
    /**
     * @inheritDoc
     */
    public function getSalt() {
        
        return $this->salt;
    }
    
    /**
     * @inheritDoc
     */
    public function getPassword() {
        return $this->password;
    }
    
    /**
     * @inheritDoc
     */
    public function getRoles()
    {
        return $this->userRoles->toArray();
    }
    
    /**
     * @see Serializable::serialize()
     */
    public function serialize() {
        return serialize([$this->id]);
    }

    /**
     * @see Serializable::unserialize()
     */
    public function unserialize($serialized) {
        list (
                $this->id,
                ) = unserialize($serialized);
    }
    
    /**
     * @param UserInterface $user
     * @return boolean
     */
    public function isEqualTo(UserInterface $user)
    {
        if (!$user instanceof User) {
            return false;
        }

        if ($this->password !== $user->getPassword()) {
            return false;
        }

        if ($this->salt !== $user->getSalt()) {
            return false;
        }

        if ($this->username !== $user->getUsername()) {
            return false;
        }

        return true;
    }
    
    /**
     * @inheritDoc
     */
    public function eraseCredentials() {
        
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set salt
     *
     * @param string $salt
     *
     * @return User
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;

        return $this;
    }


    /**
     * Add userRole
     *
     * @param UserRole $userRole
     *
     * @return User
     */
    public function addUserRole(UserRole $userRole)
    {
        $this->userRoles[] = $userRole;

        return $this;
    }

    /**
     * Remove userRole
     *
     * @param UserRole $userRole
     */
    public function removeUserRole(UserRole $userRole)
    {
        $this->userRoles->removeElement($userRole);
    }

    /**
     * Get userRoles
     *
     * @return Collection
     */
    public function getUserRoles()
    {
        return $this->userRoles;
    }
    
    /**
     * @return string
     */
    public function getPlainPassword() {
        
        return $this->plainPassword;
    }

    /**
     * @param string $password
     * @return User
     */
    public function setPlainPassword($password) {
        $this->plainPassword = $password;
        
        return $this;
    }

    /**
     * Add accessToken
     *
     * @param AccessToken $accessToken
     *
     * @return User
     */
    public function addAccessToken(AccessToken $accessToken)
    {
        $this->accessTokens[] = $accessToken;

        return $this;
    }

    /**
     * Remove accessToken
     *
     * @param AccessToken $accessToken
     */
    public function removeAccessToken(AccessToken $accessToken)
    {
        $this->accessTokens->removeElement($accessToken);
    }

    /**
     * Get accessTokens
     *
     * @return Collection
     */
    public function getAccessTokens()
    {
        return $this->accessTokens;
    }

    /**
     * Add authCode
     *
     * @param AuthCode $authCode
     *
     * @return User
     */
    public function addAuthCode(AuthCode $authCode)
    {
        $this->authCodes[] = $authCode;

        return $this;
    }

    /**
     * Remove authCode
     *
     * @param AuthCode $authCode
     */
    public function removeAuthCode(AuthCode $authCode)
    {
        $this->authCodes->removeElement($authCode);
    }

    /**
     * Get authCodes
     *
     * @return Collection
     */
    public function getAuthCodes()
    {
        return $this->authCodes;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     *
     * @return User
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return User
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return User
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set birthDate
     *
     * @param \DateTime $birthDate
     *
     * @return User
     */
    public function setBirthDate($birthDate)
    {
        $this->birthDate = $birthDate;

        return $this;
    }

    /**
     * Get birthDate
     *
     * @return \DateTime
     */
    public function getBirthDate()
    {
        return $this->birthDate;
    }

    /**
     * Set workerNumber
     *
     * @param string $workerNumber
     *
     * @return User
     */
    public function setWorkerNumber($workerNumber)
    {
        $this->workerNumber = $workerNumber;

        return $this;
    }

    /**
     * Get workerNumber
     *
     * @return string
     */
    public function getWorkerNumber()
    {
        return $this->workerNumber;
    }

    /**
     * Set workingSinceDate
     *
     * @param \DateTime $workingSinceDate
     *
     * @return User
     */
    public function setWorkingSinceDate($workingSinceDate)
    {
        $this->workingSinceDate = $workingSinceDate;

        return $this;
    }

    /**
     * Get workingSinceDate
     *
     * @return \DateTime
     */
    public function getWorkingSinceDate()
    {
        return $this->workingSinceDate;
    }

    /**
     * Set physicianTestsValidDate
     *
     * @param \DateTime $physicianTestsValidDate
     *
     * @return User
     */
    public function setPhysicianTestsValidDate($physicianTestsValidDate)
    {
        $this->physicianTestsValidDate = $physicianTestsValidDate;

        return $this;
    }

    /**
     * Get physicianTestsValidDate
     *
     * @return \DateTime
     */
    public function getPhysicianTestsValidDate()
    {
        return $this->physicianTestsValidDate;
    }

    /**
     * Set position
     *
     * @param string $position
     *
     * @return User
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return string
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return User
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set isConsulting
     *
     * @param boolean $isConsulting
     *
     * @return User
     */
    public function setIsConsulting($isConsulting)
    {
        $this->isConsulting = $isConsulting;

        return $this;
    }

    /**
     * Get isConsulting
     *
     * @return boolean
     */
    public function getIsConsulting()
    {
        return $this->isConsulting;
    }

    /**
     * Set consultationDate
     *
     * @param \DateTime $consultationDate
     *
     * @return User
     */
    public function setConsultationDate($consultationDate)
    {
        $this->consultationDate = $consultationDate;

        return $this;
    }

    /**
     * Get consultationDate
     *
     * @return \DateTime
     */
    public function getConsultationDate()
    {
        return $this->consultationDate;
    }

    /**
     * Set bankAccountNumber
     *
     * @param string $bankAccountNumber
     *
     * @return User
     */
    public function setBankAccountNumber($bankAccountNumber)
    {
        $this->bankAccountNumber = $bankAccountNumber;

        return $this;
    }

    /**
     * Get bankAccountNumber
     *
     * @return string
     */
    public function getBankAccountNumber()
    {
        return $this->bankAccountNumber;
    }

    /**
     * Set phoneNumber
     *
     * @param string $phoneNumber
     *
     * @return User
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    /**
     * Get phoneNumber
     *
     * @return string
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * Set roomNumber
     *
     * @param string $roomNumber
     *
     * @return User
     */
    public function setRoomNumber($roomNumber)
    {
        $this->roomNumber = $roomNumber;

        return $this;
    }

    /**
     * Get roomNumber
     *
     * @return string
     */
    public function getRoomNumber()
    {
        return $this->roomNumber;
    }

    /**
     * Set lab
     *
     * @param string $lab
     *
     * @return User
     */
    public function setLab($lab)
    {
        $this->lab = $lab;

        return $this;
    }

    /**
     * Get lab
     *
     * @return string
     */
    public function getLab()
    {
        return $this->lab;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return User
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set fieldOfStudies
     *
     * @param string $fieldOfStudies
     *
     * @return User
     */
    public function setFieldOfStudies($fieldOfStudies)
    {
        $this->fieldOfStudies = $fieldOfStudies;

        return $this;
    }

    /**
     * Get fieldOfStudies
     *
     * @return string
     */
    public function getFieldOfStudies()
    {
        return $this->fieldOfStudies;
    }

    /**
     * Add promotedUser
     *
     * @param User $promotedUser
     *
     * @return User
     */
    public function addPromotedUser(User $promotedUser)
    {
        $this->promotedUsers[] = $promotedUser;

        return $this;
    }

    /**
     * Remove promotedUser
     *
     * @param User $promotedUser
     */
    public function removePromotedUser(User $promotedUser)
    {
        $this->promotedUsers->removeElement($promotedUser);
    }

    /**
     * Get promotedUsers
     *
     * @return Collection
     */
    public function getPromotedUsers()
    {
        return $this->promotedUsers;
    }

    /**
     * Add promotor
     *
     * @param User $promotor
     *
     * @return User
     */
    public function addPromotor(User $promotor)
    {
        $this->promotors[] = $promotor;

        return $this;
    }

    /**
     * Remove promotor
     *
     * @param User $promotor
     */
    public function removePromotor(User $promotor)
    {
        $this->promotors->removeElement($promotor);
    }

    /**
     * Get promotors
     *
     * @return Collection
     */
    public function getPromotors()
    {
        return $this->promotors;
    }

    /**
     * Set typeOfStudies
     *
     * @param StudiesType $typeOfStudies
     *
     * @return User
     */
    public function setTypeOfStudies(StudiesType $typeOfStudies = null)
    {
        $this->typeOfStudies = $typeOfStudies;

        return $this;
    }

    /**
     * Get typeOfStudies
     *
     * @return StudiesType
     */
    public function getTypeOfStudies()
    {
        return $this->typeOfStudies;
    }

    /**
     * Set pesel
     *
     * @param string $pesel
     *
     * @return User
     */
    public function setPesel($pesel)
    {
        $this->pesel = $pesel;

        return $this;
    }

    /**
     * Get pesel
     *
     * @return string
     */
    public function getPesel()
    {
        return $this->pesel;
    }

    /**
     * Add book
     *
     * @param Book $book
     *
     * @return User
     */
    public function addBook(Book $book)
    {
        $this->books[] = $book;

        return $this;
    }

    /**
     * Remove book
     *
     * @param Book $book
     */
    public function removeBook(Book $book)
    {
        $this->books->removeElement($book);
    }

    /**
     * Get books
     *
     * @return Collection
     */
    public function getBooks()
    {
        return $this->books;
    }

    /**
     * Add publication
     *
     * @param Publication $publication
     *
     * @return User
     */
    public function addPublication(Publication $publication)
    {
        $this->publications[] = $publication;

        return $this;
    }

    /**
     * Remove publication
     *
     * @param Publication $publication
     */
    public function removePublication(Publication $publication)
    {
        $this->publications->removeElement($publication);
    }

    /**
     * Get publications
     *
     * @return Collection
     */
    public function getPublications()
    {
        return $this->publications;
    }

    /**
     * Add thesi
     *
     * @param Thesis $thesi
     *
     * @return User
     */
    public function addThesi(Thesis $thesi)
    {
        $this->thesis[] = $thesi;

        return $this;
    }

    /**
     * Remove thesi
     *
     * @param Thesis $thesi
     */
    public function removeThesi(Thesis $thesi)
    {
        $this->thesis->removeElement($thesi);
    }

    /**
     * Get thesis
     *
     * @return Collection
     */
    public function getThesis()
    {
        return $this->thesis;
    }

    /**
     * Add thesisPromotor
     *
     * @param Thesis $thesisPromotor
     *
     * @return User
     */
    public function addThesisPromotor(Thesis $thesisPromotor)
    {
        $this->thesisPromotors[] = $thesisPromotor;

        return $this;
    }

    /**
     * Remove thesisPromotor
     *
     * @param Thesis $thesisPromotor
     */
    public function removeThesisPromotor(Thesis $thesisPromotor)
    {
        $this->thesisPromotors->removeElement($thesisPromotor);
    }

    /**
     * Get thesisPromotors
     *
     * @return Collection
     */
    public function getThesisPromotors()
    {
        return $this->thesisPromotors;
    }

    /**
     * Add conference
     *
     * @param Conference $conference
     *
     * @return User
     */
    public function addConference(Conference $conference)
    {
        $this->conferences[] = $conference;

        return $this;
    }

    /**
     * Remove conference
     *
     * @param Conference $conference
     */
    public function removeConference(Conference $conference)
    {
        $this->conferences->removeElement($conference);
    }

    /**
     * Get conferences
     *
     * @return Collection
     */
    public function getConferences()
    {
        return $this->conferences;
    }

    /**
     * Add grantPaymentSourceContractor
     *
     * @param GrantPaymentSource $grantPaymentSourceContractor
     *
     * @return User
     */
    public function addGrantPaymentSourceContractor(GrantPaymentSource $grantPaymentSourceContractor)
    {
        $this->grantPaymentSourceContractors[] = $grantPaymentSourceContractor;

        return $this;
    }

    /**
     * Remove grantPaymentSourceContractor
     *
     * @param GrantPaymentSource $grantPaymentSourceContractor
     */
    public function removeGrantPaymentSourceContractor(GrantPaymentSource $grantPaymentSourceContractor)
    {
        $this->grantPaymentSourceContractors->removeElement($grantPaymentSourceContractor);
    }

    /**
     * Get grantPaymentSourceContractors
     *
     * @return Collection
     */
    public function getGrantPaymentSourceContractors()
    {
        return $this->grantPaymentSourceContractors;
    }

    /**
     * Add inventory
     *
     * @param Inventory $inventory
     *
     * @return User
     */
    public function addInventory(Inventory $inventory)
    {
        $this->inventories[] = $inventory;

        return $this;
    }

    /**
     * Remove inventory
     *
     * @param Inventory $inventory
     */
    public function removeInventory(Inventory $inventory)
    {
        $this->inventories->removeElement($inventory);
    }

    /**
     * Get inventories
     *
     * @return Collection
     */
    public function getInventories()
    {
        return $this->inventories;
    }
}
