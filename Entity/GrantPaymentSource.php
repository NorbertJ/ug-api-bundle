<?php
namespace ApiBundle\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * GrantPaymentSource
 * 
 * @ORM\Entity
 * @ORM\Table(name="grant_extends_payment_source")
 * @ORM\Entity(repositoryClass="ApiBundle\Repository\GrantPaymentSourceRepository")
 * 
 * @author Norbert Jurkiewicz <norbert.jurkiewicz@gmail.com>
 */
class GrantPaymentSource extends PaymentSource {
    
    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="manager_id", referencedColumnName="id")
     */
    protected $manager;
    
    /**
     * @ORM\ManyToMany(targetEntity="User", inversedBy="grantPaymentSourceContractors", cascade={"persist"})
     * @ORM\JoinTable(name="user_has_contractor")
     */
    protected $contractors;
    
    /**
     * @var DateTime
     *
     * @ORM\Column(type="date", nullable=true)
     */
    private $fromDate;
    
    /**
     * @var DateTime
     *
     * @ORM\Column(type="date", nullable=true)
     */
    private $toDate;
    
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $title;
    
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $number;
    
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $source;
    
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $description;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     *
     */
    protected $isDs;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $year;

    /**
     * @ORM\ManyToOne(targetEntity="IncomeOutcome")
     * @ORM\JoinColumn(name="incomeoutcome_id", referencedColumnName="id")
     */
    protected $incomeoutcome;

    /**
     * @return mixed
     */
    public function getIncomeoutcome()
    {
        return $this->incomeoutcome;
    }

    /**
     * @param $incomeoutcome
     * @return GrantPaymentSource
     */
    public function setIncomeoutcome($incomeoutcome)
    {
        $this->incomeoutcome = $incomeoutcome;

        return $this;
    }


    public function __construct() {
        $this->contractors = new ArrayCollection();
    }


    /**
     * Set fromDate
     *
     * @param \DateTime $fromDate
     *
     * @return GrantPaymentSource
     */
    public function setFromDate($fromDate)
    {
        $this->fromDate = $fromDate;

        return $this;
    }

    /**
     * Get fromDate
     *
     * @return \DateTime
     */
    public function getFromDate()
    {
        return $this->fromDate;
    }

    /**
     * Set toDate
     *
     * @param \DateTime $toDate
     *
     * @return GrantPaymentSource
     */
    public function setToDate($toDate)
    {
        $this->toDate = $toDate;

        return $this;
    }

    /**
     * Get toDate
     *
     * @return \DateTime
     */
    public function getToDate()
    {
        return $this->toDate;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return GrantPaymentSource
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set number
     *
     * @param string $number
     *
     * @return GrantPaymentSource
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set source
     *
     * @param string $source
     *
     * @return GrantPaymentSource
     */
    public function setSource($source)
    {
        $this->source = $source;

        return $this;
    }

    /**
     * Get source
     *
     * @return string
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return GrantPaymentSource
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set manager
     *
     * @param User $manager
     *
     * @return GrantPaymentSource
     */
    public function setManager(User $manager = null)
    {
        $this->manager = $manager;

        return $this;
    }

    /**
     * Get manager
     *
     * @return User
     */
    public function getManager()
    {
        return $this->manager;
    }

    /**
     * Add contractor
     *
     * @param User $contractor
     *
     * @return GrantPaymentSource
     */
    public function addContractor(User $contractor)
    {
        $this->contractors[] = $contractor;

        return $this;
    }

    /**
     * Remove contractor
     *
     * @param User $contractor
     */
    public function removeContractor(User $contractor)
    {
        $this->contractors->removeElement($contractor);
    }

    /**
     * Get contractors
     *
     * @return Collection
     */
    public function getContractors()
    {
        return $this->contractors;
    }

    /**
     * @return mixed
     */
    public function getIsDs()
    {
        return $this->isDs;

    }

    /**
     * @param mixed $isDs
     * @return  GrantPaymentSource
     */
    public function setIsDs($isDs)
    {
        $this->isDs = $isDs;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * @param mixed $year
     * @return GrantPaymentSource
     */
    public function setYear($year)
    {
        $this->year = $year;
        return $this;
    }
}
