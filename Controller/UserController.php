<?php

namespace ApiBundle\Controller;

use ApiBundle\Entity\Inventory;
use ApiBundle\Entity\User;
use ApiBundle\Form\UserForm;
use ApiBundle\Repository\BookRepository;
use ApiBundle\Repository\InventoryRepository;
use ApiBundle\Repository\UserRepository;
use JMS\DiExtraBundle\Annotation as DI;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;

/**
 * @Route("/users/")
 */
class UserController extends ApiMainController
{
    
    /**
     * @Route("")
     * @Method({"GET"})
     * @return JsonResponse
     */
    public function indexAction(Request $request)
    {
        $data = [];

        $users = $this->getUserRepository()->getAll();
        foreach ($users as $user) {
            $data[] = $this->getUserRepository()->getData($user);
        }

        return $this->getResponse($data);
    }

    /**
     * @Route("paginated/")
     * @Method({"GET"})
     * @param Request $request
     * @return JsonResponse
     */
    public function indexPaginatedAction(Request $request)
    {
        $data = [];

        $offset = $request->query->get('offset');

        if(is_null($offset))
        {
            $offset = 0;
        }

        $users = $this->getUserRepository()->getAll(true,$offset);
        foreach ($users as $user) {
            $data[] = $this->getUserRepository()->getData($user);
        }
        $data['TOTAL_AMOUNT'] = $this->getUserRepository()->countAll();
        return $this->getResponse($data);
    }
    /**
     * Find user
     *
     * @Route("find/")
     * @Method({"GET"})
     * @param Request $request
     * @return JsonResponse
     */
    public function findAction(Request $request)
    {
        $offset = $request->query->get('offset');

        if(is_null($offset))
        {
            $offset = 0;
        }

        $sSearchValue = $request->get('search_value');
        $sSearchField = $request->get('field');
        $data = [];
        $users = $this->getUserRepository()->findUser($sSearchValue, $sSearchField, $offset);
        foreach ($users['RESULTS'] as $user) {
            $data[] = $this->getUserRepository()->getData($user, 1);
        }
        $data['TOTAL_AMOUNT'] = $users['TOTAL_AMOUNT'];
        return $this->getResponse(
            $data
        );
    }
    /**
     * @param Request $request
     * @Method({"GET"})
     * @Route("authors_list/")
     * @return JsonResponse
     */
    public function getAuthorList(Request $request)
    {
        $data = array();
        $users = $this->getUserRepository()->getAllAuthors();
        foreach ($users as $user)
        {
            $data[$this->getUserRepository()->getUserId($user)] = $this->getUserRepository()->getFirstNameLastName($user);
        }

        return $this->getResponse($data);
    }

    /**
     * @param Request $request
     * @Method({"GET"})
     * @Route("workers_list/")
     * @return JsonResponse
     */
    public function getWorkersList(Request $request)
    {
        $data = array();
        $users = $this->getUserRepository()->getAllWorkers();
        foreach ($users as $user)
        {
            $data[$this->getUserRepository()->getUserId($user)] = $this->getUserRepository()->getFirstNameLastName($user);
        }

        return $this->getResponse($data);
    }

    /**
     * Get one user
     * 
     * @Route("{id}/", requirements={"id": "\d+"})
     * @Method({"GET"})
     * @ParamConverter("id", class="ApiBundle:User")
     * @param Request $request
     * @param User $user
     * @return JsonResponse
     */
    public function getAction(Request $request, User $user){
        $data = $this->getUserRepository()->getData($user, 2);
        
        return $this->getResponse($data);
    }

    /**
     * Get user by email
     *
     * @Route("getByEmail/{email}/")
     * @Method({"GET"})
     * @param Request $request
     * @param User $user
     * @return JsonResponse
     */
    public function getUserByEmailAction($email)
    {
        
        $data = $this->getUserRepository()->getUserByEmail($email);

        return $this->getResponse($data);
    }
    
    /**
     * Add new user
     * 
     * @Route("")
     * @Method({"POST"})
     * @return JsonResponse
     */
    public function addAction(){
        $user = new User();
        $form = $this->createForm(UserForm::class, $user);
        $request = $this->getFormService()->prepareRequest($form);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $this->setUserPassword($user);
            $em->persist($user);
            $em->flush();
            
            return new JsonResponse(['id' => $user->getId()], 201);
        } else {

            return $this->getFormService()->getErrorResponse($form);
        }
    }
    
    /**
     * Edit user
     * 
     * @Route("{id}/", requirements={"id": "\d+"})
     * @Method({"PUT"})
     * @ParamConverter("id", class="ApiBundle:User")
     * @param Request $request
     * @param User $user
     * @return JsonResponse
     */
    public function editAction(User $user) {
        $form = $this->createForm(UserForm::class, $user, ['method' => 'PUT', 'isEdit' => true]);
        $request = $this->getFormService()->prepareRequest($form);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $this->setUserPassword($user);
            $em->flush();

            return new JsonResponse(['id' => $user->getId()], 201);
        } else {

            return $this->getFormService()->getErrorResponse($form);
        }
    }

    /**
     * Delete concrete product
     * @Route("{id}/", requirements={"id" = "\d+"})
     * @Method({"DELETE"})
     * @ParamConverter("id", class="ApiBundle:User")
     * @param User $user
     * @return JsonResponse
     */
    public function deleteAction(User $user) {
        $em = $this->getDoctrine()->getManager();
        $em->remove($user);
        $em->flush();

        return new JsonResponse([], 204);
    }

    /**
     * @param User $user
     * @return User
     */
    private function setUserPassword(User $user) {
        $plainPassword = $user->getPlainPassword();
        if (!empty($plainPassword)) {
            $encoder = $this->getEncoderFactory()->getEncoder($user);
            $user->setPassword($encoder->encodePassword($plainPassword, $user->getSalt()));
        }

        return $user;
    }

    /**
     * Get user books and others
     *
     * @Route("getData/")
     * @Method({"GET"})
     * @return JsonResponse
     */
    public function getUserBooksAndInventories()
    {
        $User = $this->getUser();
        $aBooks = $this->getBookRepository()->getUsersBooks($User);
        $aReturnBooks = [];
        if(!empty($aBooks))
        {
            foreach ($aBooks as $book) {
                array_push($aReturnBooks,$this->getBookRepository()->getData($book));
            }
        }

        $aInventories = $this->getInventoryRepository()->getUsersInventories($User);
        $aReturnInv = [];
        if(!empty($aInventories))
        {
            foreach ($aInventories as $inventory) {
                array_push($aReturnInv,$this->getInventoryRepository()->getData($inventory));
            }
        }

        $aResponse = array(
            'BOOKS' => $aReturnBooks,
            'INVENTORY' => $aReturnInv
        );
        return new JsonResponse($aResponse, 200);

    }

    /**
     * @DI\LookupMethod("api_inventory_repository")
     * @return InventoryRepository
     */
    public function getInventoryRepository(){

    }

    /**
     * @DI\LookupMethod("api_user_repository")
     * @return UserRepository
    */
    public function getUserRepository(){
        
    }

    /**
     * @DI\LookupMethod("api_book_repository")
     * @return BookRepository
     */
    public function getBookRepository(){

    }
    
    /**
     * @DI\LookupMethod("security.encoder_factory")
     * @return EncoderFactory
    */
    public function getEncoderFactory(){
        
    }
}
