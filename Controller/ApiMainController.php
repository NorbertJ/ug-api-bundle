<?php

namespace ApiBundle\Controller;

use ApiBundle\Service\FormService;
use Doctrine\Common\Util\Debug;
use JMS\DiExtraBundle\Annotation as DI;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Profiler\Profiler;

/**
 * @author Norbert Jurkiewicz <norbert.jurkiewicz@gmail.com>
 */
abstract class ApiMainController extends Controller
{
    
    /**
     * 
     * @param array $data
     * @return JsonResponse
     */
    protected function getResponse($data){
        $response = new JsonResponse($data);
        
        return $response;
    }
    
    /**
     * @DI\LookupMethod("api_form_service")
     * @return FormService
    */
    public function getFormService(){
        
    }
    
    /**
     * @DI\LookupMethod("profiler")
     * @return Profiler
    */
    public function getProfiler(){
        
    }
}
