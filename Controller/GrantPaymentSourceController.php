<?php

namespace ApiBundle\Controller;

use ApiBundle\Entity\GrantPaymentSource;
use ApiBundle\Form\GrantPaymentSourceForm;
use ApiBundle\Repository\GrantPaymentSourceRepository;
use JMS\DiExtraBundle\Annotation as DI;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/grants/")
 * @author Norbert Jurkiewicz <norbert.jurkiewicz@gmail.com>
 */
class GrantPaymentSourceController extends ApiMainController
{
    
    /**
     * @Route("")
     * @Method({"GET"})
     * @return JsonResponse
     */
    public function indexAction()
    {
        $data = [];
        $grantPaymentSources = $this->getGrantPaymentSourceRepository()->getAll();
        foreach ($grantPaymentSources as $grantPaymentSource) {
            $data[] = $this->getGrantPaymentSourceRepository()->getData($grantPaymentSource, 2);
        }

        return $this->getResponse($data);
    }

    /**
     * @Route("paginated/")
     * @Method({"GET"})
     * @param Request $request
     * @return JsonResponse
     */
    public function indexPaginatedAction(Request $request)
    {
        $data = [];

        $offset = $request->query->get('offset');


        if(is_null($offset))
        {
            $offset = 0;
        }

        $grants = $this->getGrantPaymentSourceRepository()->getAll(true,$offset);
        foreach ($grants as $grant) {
            $data[] = $this->getGrantPaymentSourceRepository()->getData($grant, 2);
        }
        $data['TOTAL_AMOUNT'] = $this->getGrantPaymentSourceRepository()->countAll();
        return $this->getResponse($data);
    }


    /**
     * @Route("paginated/ds/")
     * @Method({"GET"})
     * @param Request $request
     * @return JsonResponse
     */
    public function indexPaginatedDsAction(Request $request)
    {
        $data = [];

        $offset = $request->query->get('offset');

        if(is_null($offset))
        {
            $offset = 0;
        }

        $grants = $this->getGrantPaymentSourceRepository()->getAll(true,$offset,true);
        foreach ($grants as $grant) {
            $data[] = $this->getGrantPaymentSourceRepository()->getData($grant, 2);
        }
        $data['TOTAL_AMOUNT'] = $this->getGrantPaymentSourceRepository()->countAll(true);
        return $this->getResponse($data);
    }

    /**
     * Find grant by title
     *
     * @Route("find/")
     * @Method({"GET"})
     * @param Request $request
     * @return JsonResponse
     */
    public function findAction(Request $request)
    {
        $offset = $request->query->get('offset');

        if(is_null($offset))
        {
            $offset = 0;
        }
        //@todo: validation empty
        $sSearchValue = $request->get('search_value');
        $sSearchField = $request->get('field');

        $data = [];
        $grants = $this->getGrantPaymentSourceRepository()->findGrant($sSearchValue, $sSearchField, $offset);
        foreach ($grants['RESULTS'] as $grant) {
            $data[] = $this->getGrantPaymentSourceRepository()->getData($grant, 2);

        }

        $data['TOTAL_AMOUNT'] = $grants['TOTAL_AMOUNT'];

        return $this->getResponse($data);
    }

    /**
     * Get one grantPaymentSource
     * 
     * @Route("{id}/", requirements={"id": "\d+"})
     * @Method({"GET"})
     * @ParamConverter("id", class="ApiBundle:GrantPaymentSource")
     * @param Request $request
     * @param GrantPaymentSource $grantPaymentSource
     * @return JsonResponse
     */
    public function getAction(GrantPaymentSource $grantPaymentSource){
        $data = $this->getGrantPaymentSourceRepository()->getData($grantPaymentSource, 2);
        
        return $this->getResponse($data);
    }
    
    /**
     * Add new grantPaymentSource
     * 
     * @Route("")
     * @Method({"POST"})
     * @return JsonResponse
     */
    public function addAction(){
        $grantPaymentSource = new GrantPaymentSource();
        $form = $this->createForm(GrantPaymentSourceForm::class, $grantPaymentSource);
        $request = $this->getFormService()->prepareRequest($form);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($grantPaymentSource);
            $em->flush();
            
            return new JsonResponse(['id' => $grantPaymentSource->getId()], 201);
        } else {

            return $this->getFormService()->getErrorResponse($form);
        }
    }
    
    /**
     * Edit grantPaymentSource
     * 
     * @Route("{id}/", requirements={"id": "\d+"})
     * @Method({"PUT"})
     * @ParamConverter("id", class="ApiBundle:GrantPaymentSource")
     * @param GrantPaymentSource $grantPaymentSource
     * @return JsonResponse
     */
    public function editAction(GrantPaymentSource $grantPaymentSource) {
        $form = $this->createForm(GrantPaymentSourceForm::class, $grantPaymentSource, ['method' => 'PUT']);
        $request = $this->getFormService()->prepareRequest($form);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->flush();

            return new JsonResponse(['id' => $grantPaymentSource->getId()], 201);
        } else {

            return $this->getFormService()->getErrorResponse($form);
        }
    }

    /**
     * Increase decrease
     *
     * @Route("{id}/changeCurrent/", requirements={"id": "\d+"})
     * @Method({"GET"})
     * @ParamConverter("id", class="ApiBundle:GrantPaymentSource")
     * @param GrantPaymentSource $grantPaymentSource
     * @param Request $request
     * @return JsonResponse
     */
    public function changeCurrentAction(GrantPaymentSource $grantPaymentSource, Request $request)
    {

        $amount = $request->get('amount');

        if(!is_numeric($amount))
        {
            return new JsonResponse(['error' => 'Incorect amount'], 400);
        }

        $current = $grantPaymentSource->getCurrentValue();
        $new = $current - $amount;
        $grantPaymentSource->setCurrentValue($new);
        try {
            $em = $this->getDoctrine()->getManager();
            $em->flush();
        } catch (\Exception $e)
        {
            return new JsonResponse(['error' => $e->getMessage()], 404);
        }


        return new JsonResponse(['id' => $grantPaymentSource->getId()], 201);

    }

    /**
     * Delete concrete product
     * @Route("{id}/", requirements={"id" = "\d+"})
     * Method({"DELETE"})
     * @ParamConverter("id", class="ApiBundle:GrantPaymentSource")
     * @param GrantPaymentSource $grantPaymentSource
     * @return JsonResponse
     */
    public function deleteAction(GrantPaymentSource $grantPaymentSource) {
        $em = $this->getDoctrine()->getManager();
        $em->remove($grantPaymentSource);
        $em->flush();

        return new JsonResponse([], 204);
    }
    
    /**
     * @DI\LookupMethod("api_grantPaymentSource_repository")
     * @return GrantPaymentSourceRepository
    */
    public function getGrantPaymentSourceRepository(){
        
    }

}
