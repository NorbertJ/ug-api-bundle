<?php

namespace ApiBundle\Controller;

use ApiBundle\Entity\Conference;
use ApiBundle\Form\ConferenceForm;
use ApiBundle\Repository\ConferenceRepository;
use JMS\DiExtraBundle\Annotation as DI;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/conferences/")
 * @author Norbert Jurkiewicz <norbert.jurkiewicz@gmail.com>
 */
class ConferenceController extends ApiMainController
{
    
    /**
     * @Route("")
     * @Method({"GET"})
     * @return JsonResponse
     */
    public function indexAction()
    {
        $data = [];
        $conferences = $this->getConferenceRepository()->getAll();
        foreach ($conferences as $conference) {
            $data[] = $this->getConferenceRepository()->getData($conference, 2);
        }

        return $this->getResponse($data);
    }

    /**
     * @Route("paginated/")
     * @Method({"GET"})
     * @param Request $request
     * @return JsonResponse
     */
    public function indexPaginatedAction(Request $request)
    {
        $data = [];
        $offset = $request->query->get('offset');
        if(is_null($offset))
        {
            $offset = 0;
        }

        $books = $this->getConferenceRepository()->getAll(true,$offset);
        foreach ($books as $book) {
            $data[] = $this->getConferenceRepository()->getData($book, 2);
        }
        $data['TOTAL_AMOUNT'] = $this->getConferenceRepository()->countAll();
        return $this->getResponse($data);
    }
    /**
     * Get one conference
     * 
     * @Route("{id}/", requirements={"id": "\d+"})
     * @Method({"GET"})
     * @ParamConverter("id", class="ApiBundle:Conference")
     * @param Conference $conference
     * @return JsonResponse
     */
    public function getAction(Conference $conference){
        $data = $this->getConferenceRepository()->getData($conference, 2);
        
        return $this->getResponse($data);
    }
    
    /**
     * Add new conference
     * 
     * @Route("")
     * @Method({"POST"})
     * @return JsonResponse
     */
    public function addAction(){
        $conference = new Conference();
        $form = $this->createForm(ConferenceForm::class, $conference);
        $request = $this->getFormService()->prepareRequest($form);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($conference);
            $em->flush();
            
            return new JsonResponse(['id' => $conference->getId()], 201);
        } else {

            return $this->getFormService()->getErrorResponse($form);
        }
    }



    /**
     * Find conference by title
     *
     * @Route("find/")
     * @Method({"GET"})
     * @param Request $request
     * @return JsonResponse
     */
    public function findAction(Request $request)
    {
        $offset = $request->query->get('offset');

        if(is_null($offset))
        {
            $offset = 0;
        }
        $sSearchValue = $request->get('search_value');
        $sSearchField = $request->get('field');
        $data = [];
        $conferences = $this->getConferenceRepository()->findConference($sSearchValue, $sSearchField,$offset);
        foreach ($conferences['RESULTS'] as $conference) {
            $data[] = $this->getConferenceRepository()->getData($conference, 2);
        }

        $data['TOTAL_AMOUNT'] = $conferences['TOTAL_AMOUNT'];

        return $this->getResponse($data);
    }


    /**
     * Edit conference
     * 
     * @Route("{id}/", requirements={"id": "\d+"})
     * @Method({"PUT"})
     * @ParamConverter("id", class="ApiBundle:Conference")
     * @param Request $request
     * @param Conference $conference
     * @return JsonResponse
     */
    public function editAction(Conference $conference) {
        $form = $this->createForm(ConferenceForm::class, $conference, ['method' => 'PUT']);
        $request = $this->getFormService()->prepareRequest($form);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->flush();

            return new JsonResponse(['id' => $conference->getId()], 201);
        } else {

            return $this->getFormService()->getErrorResponse($form);
        }
    }

    /**
     * Delete concrete product
     * @Route("{id}/", requirements={"id" = "\d+"})
     * Method({"DELETE"})
     * @ParamConverter("id", class="ApiBundle:Conference")
     * @param Conference $conference
     * @return JsonResponse
     */
    public function deleteAction(Conference $conference) {
        $em = $this->getDoctrine()->getManager();
        $em->remove($conference);
        $em->flush();

        return new JsonResponse([], 204);
    }
    
    /**
     * @DI\LookupMethod("api_conference_repository")
     * @return ConferenceRepository
    */
    public function getConferenceRepository(){
        
    }

}
