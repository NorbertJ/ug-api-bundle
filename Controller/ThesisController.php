<?php

namespace ApiBundle\Controller;

use ApiBundle\Entity\Thesis;
use ApiBundle\Form\ThesisForm;
use ApiBundle\Repository\ThesisRepository;
use JMS\DiExtraBundle\Annotation as DI;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/theses/")
 * @author Norbert Jurkiewicz <norbert.jurkiewicz@gmail.com>
 */
class ThesisController extends ApiMainController
{
    
    /**
     * @Route("")
     * @Method({"GET"})
     * @return JsonResponse
     */
    public function indexAction()
    {
        $data = [];
        $thesiss = $this->getThesisRepository()->getAll();
        foreach ($thesiss as $thesis) {
            $data[] = $this->getThesisRepository()->getData($thesis, 2);
        }

        return $this->getResponse($data);
    }

    /**
     * @Route("paginated/")
     * @Method({"GET"})
     * @param Request $request
     * @return JsonResponse
     */
    public function indexPaginatedAction(Request $request)
    {
        $data = [];

        $offset = $request->query->get('offset');


        if(is_null($offset))
        {
            $offset = 0;
        }

        $thesis = $this->getThesisRepository()->getAll(true,$offset);
        foreach ($thesis as $thes) {
            $data[] = $this->getThesisRepository()->getData($thes, 2);
        }
        $data['TOTAL_AMOUNT'] = $this->getThesisRepository()->countAll();
        return $this->getResponse($data);
    }

    /**
     * Find books by title
     *
     * @Route("find/")
     * @Method({"GET"})
     * @param Request $request
     * @return JsonResponse
     */
    public function findAction(Request $request)
    {
        $offset = $request->query->get('offset');

        if(is_null($offset))
        {
            $offset = 0;
        }
        //@todo: validation empty
        $sSearchValue = $request->get('search_value');
        $sSearchField = $request->get('field');

        $data = [];
        $thesis = $this->getThesisRepository()->findThesis($sSearchValue, $sSearchField, $offset);
        foreach ($thesis['RESULTS'] as $thes) {
            $data[] = $this->getThesisRepository()->getData($thes, 2);
        }

        $data['TOTAL_AMOUNT'] = $thesis['TOTAL_AMOUNT'];

        return $this->getResponse($data);
    }
    /**
     * Get one thesis
     * 
     * @Route("{id}/", requirements={"id": "\d+"})
     * @Method({"GET"})
     * @ParamConverter("id", class="ApiBundle:Thesis")
     * @param Request $request
     * @param Thesis $thesis
     * @return JsonResponse
     */
    public function getAction(Thesis $thesis){
        $data = $this->getThesisRepository()->getData($thesis, 2);
        
        return $this->getResponse($data);
    }
    
    /**
     * Add new thesis
     * 
     * @Route("")
     * @Method({"POST"})
     * @return JsonResponse
     */
    public function addAction(){
        $thesis = new Thesis();
        $form = $this->createForm(ThesisForm::class, $thesis);
        $request = $this->getFormService()->prepareRequest($form);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($thesis);
            $em->flush();
            
            return new JsonResponse(['id' => $thesis->getId()], 201);
        } else {

            return $this->getFormService()->getErrorResponse($form);
        }
    }
    
    /**
     * Edit thesis
     * 
     * @Route("{id}/", requirements={"id": "\d+"})
     * @Method({"PUT"})
     * @ParamConverter("id", class="ApiBundle:Thesis")
     * @param Request $request
     * @param Thesis $thesis
     * @return JsonResponse
     */
    public function editAction(Thesis $thesis) {
        $form = $this->createForm(ThesisForm::class, $thesis, ['method' => 'PUT']);
        $request = $this->getFormService()->prepareRequest($form);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->flush();

            return new JsonResponse(['id' => $thesis->getId()], 201);
        } else {

            return $this->getFormService()->getErrorResponse($form);
        }
    }

    /**
     * Delete concrete product
     * @Route("{id}/", requirements={"id" = "\d+"})
     * @Method({"DELETE"})
     * @ParamConverter("id", class="ApiBundle:Thesis")
     * @param Thesis $thesis
     * @return JsonResponse
     */
    public function deleteAction(Thesis $thesis) {
        $em = $this->getDoctrine()->getManager();
        $em->remove($thesis);
        $em->flush();

        return new JsonResponse([], 204);
    }
    
    /**
     * @DI\LookupMethod("api_thesis_repository")
     * @return ThesisRepository
    */
    public function getThesisRepository(){
        
    }

}
