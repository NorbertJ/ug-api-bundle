<?php

namespace ApiBundle\Controller;

use ApiBundle\Entity\Invoice;
use ApiBundle\Form\InvoiceForm;
use ApiBundle\Repository\InvoiceRepository;
use JMS\DiExtraBundle\Annotation as DI;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/invoices/")
 * @author Norbert Jurkiewicz <norbert.jurkiewicz@gmail.com>
 */
class InvoiceController extends ApiMainController
{
    
    /**
     * Get all invoices
     * 
     * @Route("")
     * @Method({"GET"})
     * @return JsonResponse
     */
    public function indexAction()
    {
        $data = [];
        $invoices = $this->getInvoiceRepository()->getAll();
        foreach ($invoices as $invoice) {
            $data[] = $this->getInvoiceRepository()->getData($invoice, 2);
        }

        return $this->getResponse($data);
    }

    /**
     * @Route("paginated/")
     * @Method({"GET"})
     * @param Request $request
     * @return JsonResponse
     */
    public function indexPaginatedAction(Request $request)
    {
        $data = [];

        $offset = $request->query->get('offset');


        if(is_null($offset))
        {
            $offset = 0;
        }

        $invoices = $this->getInvoiceRepository()->getAll(true,$offset);
        foreach ($invoices as $invoice) {
            $data[] = $this->getInvoiceRepository()->getData($invoice, 2);
        }
        $data['TOTAL_AMOUNT'] = $this->getInvoiceRepository()->countAll();
        return $this->getResponse($data);
    }
    /**
     * Find invoice
     *
     * @Route("find/")
     * @Method({"GET"})
     * @param Request $request
     * @return JsonResponse
     */
    public function findAction(Request $request)
    {
        $offset = $request->query->get('offset');

        if(is_null($offset))
        {
            $offset = 0;
        }
        //@todo: validation empty
        $sSearchValue = $request->get('search_value');
        $sSearchField = $request->get('field');

        $data = [];
        $invoices = $this->getInvoiceRepository()->findInvoice($sSearchValue, $sSearchField, $offset);
        foreach ($invoices['RESULTS'] as $invoice) {
            $data[] = $this->getInvoiceRepository()->getData($invoice, 2);
        }

        $data['TOTAL_AMOUNT'] = $invoices['TOTAL_AMOUNT'];

        return $this->getResponse($data);
    }
    /**
     * Get one invoice
     * 
     * @Route("{id}/", requirements={"id": "\d+"})
     * @Method({"GET"})
     * @ParamConverter("id", class="ApiBundle:Invoice")
     * @param Request $request
     * @param Invoice $invoice
     * @return JsonResponse
     */
    public function getAction(Invoice $invoice){
        $data = $this->getInvoiceRepository()->getData($invoice, 2);
        
        return $this->getResponse($data);
    }
    
    /**
     * Add new invoice
     * 
     * @Route("")
     * @Method({"POST"})
     * @return JsonResponse
     */
    public function addAction(){
        $invoice = new Invoice();
        $form = $this->createForm(InvoiceForm::class, $invoice);
        $request = $this->getFormService()->prepareRequest($form);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($invoice);
            $em->flush();
            
            return new JsonResponse(['id' => $invoice->getId()], 201);
        } else {

            return $this->getFormService()->getErrorResponse($form);
        }
    }
    
    /**
     * Edit invoice
     * 
     * @Route("{id}/", requirements={"id": "\d+"})
     * @Method({"PUT"})
     * @ParamConverter("id", class="ApiBundle:Invoice")
     * @param Request $request
     * @param Invoice $invoice
     * @return JsonResponse
     */
    public function editAction(Invoice $invoice) {
        $form = $this->createForm(InvoiceForm::class, $invoice, ['method' => 'PUT']);
        $request = $this->getFormService()->prepareRequest($form);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->flush();

            return new JsonResponse(['id' => $invoice->getId()], 201);
        } else {

            return $this->getFormService()->getErrorResponse($form);
        }
    }

    /**
     * Delete concrete product
     * @Route("{id}/", requirements={"id" = "\d+"})
     * Method({"DELETE"})
     * @ParamConverter("id", class="ApiBundle:Invoice")
     * @param Invoice $invoice
     * @return JsonResponse
     */
    public function deleteAction(Invoice $invoice) {
        $em = $this->getDoctrine()->getManager();
        $em->remove($invoice);
        $em->flush();

        return new JsonResponse([], 204);
    }
    
    /**
     * @DI\LookupMethod("api_invoice_repository")
     * @return InvoiceRepository
    */
    public function getInvoiceRepository(){
        
    }

}
