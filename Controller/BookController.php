<?php

namespace ApiBundle\Controller;

use ApiBundle\Entity\Book;
use ApiBundle\Entity\User;
use ApiBundle\Form\BookForm;
use ApiBundle\Repository\BookRepository;
use JMS\DiExtraBundle\Annotation as DI;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * @Route("/books/")
 * @author Norbert Jurkiewicz <norbert.jurkiewicz@gmail.com>
 */
class BookController extends ApiMainController
{


    /**
     * @Route("")
     * @Method({"GET"})
     * @return JsonResponse
     */
    public function indexAction()
    {
        $data = [];

        $books = $this->getBookRepository()->getAll();
        foreach ($books as $book) {
            $data[] = $this->getBookRepository()->getData($book, 2);
        }

        return $this->getResponse($data);
    }

    /**
     * @Route("paginated/")
     * @Method({"GET"})
     * @param Request $request
     * @return JsonResponse
     */
    public function indexPaginatedAction(Request $request)
    {
        $data = [];

        $offset = $request->query->get('offset');


        if(is_null($offset))
        {
            $offset = 0;
        }

        $books = $this->getBookRepository()->getAll(true,$offset);
        foreach ($books as $book) {
            $data[] = $this->getBookRepository()->getData($book, 2);
        }
        $data['TOTAL_AMOUNT'] = $this->getBookRepository()->countAll();
        return $this->getResponse($data);
    }
    /**
     * Get one book
     * 
     * @Route("{id}/", requirements={"id": "\d+"})
     * @Method({"GET"})
     * @ParamConverter("id", class="ApiBundle:Book")
     * @param Book $book
     * @return JsonResponse
     */
    public function getAction(Book $book){
        $data = $this->getBookRepository()->getData($book, 2);
        
        return $this->getResponse($data);
    }

    /**
     * Find books by title
     *
     * @Route("find/")
     * @Method({"GET"})
     * @param Request $request
     * @return JsonResponse
     */
    public function findAction(Request $request)
    {
        $offset = $request->query->get('offset');

        if(is_null($offset))
        {
            $offset = 0;
        }
        //@todo: validation empty
        $sSearchValue = $request->get('search_value');
        $sSearchField = $request->get('field');

        $data = [];
        $books = $this->getBookRepository()->findBook($sSearchValue, $sSearchField, $offset);
        foreach ($books['RESULTS'] as $book) {
            $data[] = $this->getBookRepository()->getData($book, 2);
        }

        $data['TOTAL_AMOUNT'] = $books['TOTAL_AMOUNT'];

        return $this->getResponse($data);
    }
    
    /**
     * Add new book
     * 
     * @Route("")
     * @Method({"POST"})
     * @return JsonResponse
     */
    public function addAction(){
        $book = new Book();
        $form = $this->createForm(BookForm::class, $book);
        $request = $this->getFormService()->prepareRequest($form);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($book);
            $em->flush();
            
            return new JsonResponse(['id' => $book->getId()], 201);
        } else {

            return $this->getFormService()->getErrorResponse($form);
        }
    }
    
    /**
     * Edit book
     * 
     * @Route("{id}/", requirements={"id": "\d+"})
     * @Method({"PUT"})
     * @ParamConverter("id", class="ApiBundle:Book")
     * @param Book $book
     * @return JsonResponse
     */
    public function editAction(Book $book) {
        $form = $this->createForm(BookForm::class, $book, ['method' => 'PUT']);
        $request = $this->getFormService()->prepareRequest($form);
        $form->handleRequest($request);
        
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->flush();

            return new JsonResponse(['id' => $book->getId()], 201);
        } else {

            return $this->getFormService()->getErrorResponse($form);
        }
    }

    /**
     * Borrow book
     *
     * @Route("{id}/borrow/", requirements={"id": "\d+"})
     * @Method({"PUT"})
     * @ParamConverter("id", class="ApiBundle:Book")
     * @param Book $book
     * @param Request $request
     * @return JsonResponse
     */
    public function borrowAction(Book $book, Request $request)
    {
        if($book->getIsBorrowed())
        {
            return new JsonResponse(['id' => $book->getId()], 404);
        }
        //@Todo: validation

        $date = $request->get('returnDate');
        $returnDate =  \DateTime::createFromFormat('Y-m-d', $date);

        $book->setIsBorrowed(true);
        $book->setOwner($this->getUser());
        $book->setReturnDate($returnDate);

        $em = $this->getDoctrine()->getManager();
        $em->flush();

        return new JsonResponse(['id' => $book->getId()], 201);

    }

    /**
     *
     * Return book
     *
     *
     * @Route("{id}/return/", requirements={"id": "\d+"})
     * @Method({"PUT"})
     * @ParamConverter("id", class="ApiBundle:Book")
     * @param Book $book
     * @return JsonResponse
     */
    public function returnAction(Book $book)
    {
        if(!$book->getIsBorrowed())
        {
            return new JsonResponse(['id' => $book->getId()], 404);
        }
        $book->setIsBorrowed(false);
        $book->setOwner(null);
        $book->setReturnDate(null);

        $em = $this->getDoctrine()->getManager();
        $em->flush();

        return new JsonResponse(['id' => $book->getId()], 201);
    }

    /**
     * Delete concrete product
     * @Route("{id}/", requirements={"id" = "\d+"})
     * @Method({"DELETE"})
     * @ParamConverter("id", class="ApiBundle:Book")
     * @param Book $book
     * @return JsonResponse
     */
    public function deleteAction(Book $book) {
        $em = $this->getDoctrine()->getManager();
        $em->remove($book);
        $em->flush();

        return new JsonResponse([], 204);
    }
    
    /**
     * @DI\LookupMethod("api_book_repository")
     * @return BookRepository
    */
    public function getBookRepository(){
        
    }

}
