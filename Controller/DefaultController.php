<?php

namespace ApiBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


/**
 * @Route("/")
 */
class DefaultController extends Controller
{
    
    /**
     * @Route("/")
     * @Template()
     */
    public function indexAction()
    {
        
        return [];
    }
}
