<?php

namespace ApiBundle\Controller;

use ApiBundle\Entity\Book;
use ApiBundle\Form\BookForm;
use ApiBundle\Repository\ThesisRepository;
use ApiBundle\Repository\UserRepository;
use JMS\DiExtraBundle\Annotation as DI;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Only for tests!
 * 
 * @Route("/test/")
 * @author Norbert Jurkiewicz <norbert.jurkiewicz@gmail.com>
 */
class TestController extends Controller
{
    
    /**
     * @Route("/")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $data = [];
        $t = $this->get('api_user_repository')->getAll();
        foreach ($t as $a) {
            $data[] = $this->get('api_user_repository')->getData($a, 3);
        }
        
        return ['data' => $data];
    }
    
    /**
     * @DI\LookupMethod("api_thesis_repository")
     * @return ThesisRepository
    */
    public function getThesisRepository(){
        
    }
    
}
