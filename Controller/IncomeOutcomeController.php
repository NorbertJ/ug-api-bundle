<?php
/**
 * File Description
 *
 * Some comprehensive Description of the files contents
 *
 * @author Norbert Jurkiewicz <norbert.jurkiewicz@interactivedata.com>
 * @user norbert
 * @version $Author:$ $Id:$ $Revision:$ $Date:$
 * @copyright 2016 Interactive Data Managed Solutions AG
 * @since 26.09.2016
 */
namespace ApiBundle\Controller;

use ApiBundle\Entity\GrantPaymentSource;
use ApiBundle\Entity\IncomeOutcome;
use ApiBundle\Entity\Book;
use ApiBundle\Entity\Invoice;
use ApiBundle\Entity\User;
use ApiBundle\Form\IncomeOutcomeForm;
use ApiBundle\Repository\BookRepository;
use ApiBundle\Repository\IncomeOutcomeRepository;
use JMS\DiExtraBundle\Annotation as DI;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * Class IncomeOutcomeController
 * @package ApiBundle\Controller
 * @Route("/incomeoutcome/")
 */
class IncomeOutcomeController extends ApiMainController
{
    /**
     * @Route("")
     * @Method({"GET"})
     * @return JsonResponse
     */
    public function indexAction()
    {
        $incomeOutcome = $this->getIncomeOutcomeRepository()->getAll();
        $data = [];
        foreach ($incomeOutcome as $item)
        {
            $data[] = $this->getIncomeOutcomeRepository()->getData($item);
        }
        return $this->getResponse($data);
    }


    /**
     * Add new Income Outcome
     *
     * @Route("")
     * @Method({"POST"})
     * @return JsonResponse
     */
    public function addAction(){
        $incomeOutcome = new IncomeOutcome();
        $form = $this->createForm(IncomeOutcomeForm::class, $incomeOutcome);
        $request = $this->getFormService()->prepareRequest($form);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($incomeOutcome);
            $em->flush();

            return new JsonResponse(['id' => $incomeOutcome->getId()], 201);
        } else {

            return $this->getFormService()->getErrorResponse($form);
        }
    }

    /**
     * Edit Income outcome
     *
     * @Route("{id}/", requirements={"id": "\d+"})
     * @Method({"PUT"})
     * @ParamConverter("id", class="ApiBundle:IncomeOutcome")
     * @param IncomeOutcome $incomeOutcome
     * @return JsonResponse
     */
    public function editAction(IncomeOutcome $incomeOutcome) {
        $form = $this->createForm(IncomeOutcomeForm::class, $incomeOutcome, ['method' => 'PUT']);
        $request = $this->getFormService()->prepareRequest($form);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->flush();

            return new JsonResponse(['id' => $incomeOutcome->getId()], 201);
        } else {

            return $this->getFormService()->getErrorResponse($form);
        }
    }

    /**
     * Get Income Outcome for Invoice
     * @Route("invoice/{id}/", requirements={"id": "\d+"})
     * @Method({"GET"})
     * @ParamConverter("id", class="ApiBundle:Invoice")
     * @param Invoice $invoice
     * @return JsonResponse
     */
    public function invoiceAction(Invoice $invoice)
    {
        $aIncomeOutcome = $this->getIncomeOutcomeRepository()->getForInvoice($invoice);
        $data = [];
        foreach ($aIncomeOutcome as $item)
        {
            $data[] = $this->getIncomeOutcomeRepository()->getData($item);
        }
        return new JsonResponse($data, 200);
    }

    /**
     * Get Income Outcome for GrantPaymentSource
     * @Route("grantpaymentsource/{id}/", requirements={"id": "\d+"})
     * @Method({"GET"})
     * @ParamConverter("id", class="ApiBundle:GrantPaymentSource")
     * @param GrantPaymentSource $grantPaymentSource
     * @return JsonResponse
     */
    public function grantPaymentSourceAction(GrantPaymentSource $grantPaymentSource)
    {
        $aIncomeOutcome = $this->getIncomeOutcomeRepository()->getForPayment($grantPaymentSource);
        $data = [];
        foreach ($aIncomeOutcome as $item)
        {
            $data[] = $this->getIncomeOutcomeRepository()->getData($item);
        }
        return new JsonResponse($data, 200);
    }

    /**
     * Delete concrete product
     * @Route("{id}/", requirements={"id" = "\d+"})
     * @Method({"DELETE"})
     * @ParamConverter("id", class="ApiBundle:IncomeOutcome")
     * @param IncomeOutcome $incomeOutcome
     * @return JsonResponse
     */
    public function deleteAction(IncomeOutcome $incomeOutcome) {
        $em = $this->getDoctrine()->getManager();
        $em->remove($incomeOutcome);
        $em->flush();

        return new JsonResponse([], 204);
    }

    /**
     * @DI\LookupMethod("api_incomeoutcome_repository")
     * @return IncomeOutcomeRepository
     */
    public function getIncomeOutcomeRepository(){

    }
}
