<?php

namespace ApiBundle\Controller;

use ApiBundle\Entity\Publication;
use ApiBundle\Form\PublicationForm;
use ApiBundle\Repository\PublicationRepository;
use JMS\DiExtraBundle\Annotation as DI;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/publications/")
 * @author Norbert Jurkiewicz <norbert.jurkiewicz@gmail.com>
 */
class PublicationController extends ApiMainController
{
    
    /**
     * @Route("")
     * @Method({"GET"})
     * @return JsonResponse
     */
    public function indexAction()
    {
        $data = [];
        $publications = $this->getPublicationRepository()->getAll();
        foreach ($publications as $publication) {
            $data[] = $this->getPublicationRepository()->getData($publication, 2);
        }

        return $this->getResponse($data);
    }
    /**
     * @Route("paginated/")
     * @Method({"GET"})
     * @param Request $request
     * @return JsonResponse
     */
    public function indexPaginatedAction(Request $request)
    {
        $data = [];

        $offset = $request->query->get('offset');


        if(is_null($offset))
        {
            $offset = 0;
        }

        $publications = $this->getPublicationRepository()->getAll(true,$offset);
        foreach ($publications as $publication) {
            $data[] = $this->getPublicationRepository()->getData($publication, 2);
        }
        $data['TOTAL_AMOUNT'] = $this->getPublicationRepository()->countAll();
        return $this->getResponse($data);
    }

    /**
     * Find publication
     *
     * @Route("find/")
     * @Method({"GET"})
     * @param Request $request
     * @return JsonResponse
     */
    public function findAction(Request $request)
    {
        $offset = $request->query->get('offset');

        if(is_null($offset))
        {
            $offset = 0;
        }
        //@todo: validation empty
        $sSearchValue = $request->get('search_value');
        $sSearchField = $request->get('field');

        $data = [];
        $publications = $this->getPublicationRepository()->findPublication($sSearchValue, $sSearchField, $offset);
        foreach ($publications['RESULTS'] as $publication) {
            $data[] = $this->getPublicationRepository()->getData($publication, 2);
        }

        $data['TOTAL_AMOUNT'] = $publications['TOTAL_AMOUNT'];

        return $this->getResponse($data);
    }
    
    /**
     * Get one publication
     * 
     * @Route("{id}/", requirements={"id": "\d+"})
     * @Method({"GET"})
     * @ParamConverter("id", class="ApiBundle:Publication")
     * @param Request $request
     * @param Publication $publication
     * @return JsonResponse
     */
    public function getAction(Publication $publication){
        $data = $this->getPublicationRepository()->getData($publication, 2);
        
        return $this->getResponse($data);
    }
    
    /**
     * Add new publication
     * 
     * @Route("")
     * @Method({"POST"})
     * @return JsonResponse
     */
    public function addAction(){
        $publication = new Publication();
        $form = $this->createForm(PublicationForm::class, $publication);
        $request = $this->getFormService()->prepareRequest($form);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($publication);
            $em->flush();
            
            return new JsonResponse(['id' => $publication->getId()], 201);
        } else {

            return $this->getFormService()->getErrorResponse($form);
        }
    }
    
    /**
     * Edit publication
     * 
     * @Route("{id}/", requirements={"id": "\d+"})
     * @Method({"PUT"})
     * @ParamConverter("id", class="ApiBundle:Publication")
     * @param Request $request
     * @param Publication $publication
     * @return JsonResponse
     */
    public function editAction(Publication $publication) {
        $form = $this->createForm(PublicationForm::class, $publication, ['method' => 'PUT']);
        $request = $this->getFormService()->prepareRequest($form);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->flush();

            return new JsonResponse(['id' => $publication->getId()], 201);
        } else {

            return $this->getFormService()->getErrorResponse($form);
        }
    }

    /**
     * Delete concrete product
     * @Route("{id}/", requirements={"id" = "\d+"})
     * Method({"DELETE"})
     * @ParamConverter("id", class="ApiBundle:Publication")
     * @param Publication $publication
     * @return JsonResponse
     */
    public function deleteAction(Publication $publication) {
        $em = $this->getDoctrine()->getManager();
        $em->remove($publication);
        $em->flush();

        return new JsonResponse([], 204);
    }
    
    /**
     * @DI\LookupMethod("api_publication_repository")
     * @return PublicationRepository
    */
    public function getPublicationRepository(){
        
    }

}
