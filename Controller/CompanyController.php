<?php

namespace ApiBundle\Controller;

use ApiBundle\Entity\Company;
use ApiBundle\Form\CompanyForm;
use ApiBundle\Repository\CompanyRepository;
use JMS\DiExtraBundle\Annotation as DI;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/companies/")
 * @author Norbert Jurkiewicz <norbert.jurkiewicz@gmail.com>
 */
class CompanyController extends ApiMainController
{
    
    /**
     * Get all companies
     * 
     * @Route("")
     * @Method({"GET"})
     * @return JsonResponse
     */
    public function indexAction()
    {
        $data = [];
        $companys = $this->getCompanyRepository()->findAll();
        foreach ($companys as $company) {
            $data[] = $this->getCompanyRepository()->getData($company, 2);
        }

        return $this->getResponse($data);
    }

    /**
     * @Route("paginated/")
     * @Method({"GET"})
     * @param Request $request
     * @return JsonResponse
     */
    public function indexPaginatedAction(Request $request)
    {
        $data = [];

        $offset = $request->query->get('offset');


        if(is_null($offset))
        {
            $offset = 0;
        }

        $companies = $this->getCompanyRepository()->getAll(true,$offset);
        foreach ($companies as $company) {
            $data[] = $this->getCompanyRepository()->getData($company);
        }
        $data['TOTAL_AMOUNT'] = $this->getCompanyRepository()->countAll();
        return $this->getResponse($data);
    }

    /**
     * Find books by title
     *
     * @Route("find/")
     * @Method({"GET"})
     * @param Request $request
     * @return JsonResponse
     */
    public function findAction(Request $request)
    {
        $offset = $request->query->get('offset');

        if(is_null($offset))
        {
            $offset = 0;
        }
        //@todo: validation empty
        $sSearchValue = $request->get('search_value');
        $sSearchField = $request->get('field');

        $data = [];
        $companies = $this->getCompanyRepository()->findCompany($sSearchValue, $sSearchField, $offset);
        foreach ($companies['RESULTS'] as $company) {
            $data[] = $this->getCompanyRepository()->getData($company);
        }

        $data['TOTAL_AMOUNT'] = $companies['TOTAL_AMOUNT'];

        return $this->getResponse($data);
    }
    
    /**
     * Get one company
     * 
     * @Route("{id}/", requirements={"id": "\d+"})
     * @Method({"GET"})
     * @ParamConverter("id", class="ApiBundle:Company")
     * @param Request $request
     * @param Company $company
     * @return JsonResponse
     */
    public function getAction(Company $company){
        $data = $this->getCompanyRepository()->getData($company, 2);
        
        return $this->getResponse($data);
    }
    
    /**
     * Add new company
     * 
     * @Route("")
     * @Method({"POST"})
     * @return JsonResponse
     */
    public function addAction(){
        $company = new Company();
        $form = $this->createForm(CompanyForm::class, $company);
        $request = $this->getFormService()->prepareRequest($form);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($company);
            $em->flush();
            
            return new JsonResponse(['id' => $company->getId()], 201);
        } else {

            return $this->getFormService()->getErrorResponse($form);
        }
    }
    
    /**
     * Edit company
     * 
     * @Route("{id}/", requirements={"id": "\d+"})
     * @Method({"PUT"})
     * @ParamConverter("id", class="ApiBundle:Company")
     * @param Request $request
     * @param Company $company
     * @return JsonResponse
     */
    public function editAction(Company $company) {
        $form = $this->createForm(CompanyForm::class, $company, ['method' => 'PUT']);
        $request = $this->getFormService()->prepareRequest($form);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->flush();

            return new JsonResponse(['id' => $company->getId()], 201);
        } else {

            return $this->getFormService()->getErrorResponse($form);
        }
    }

    /**
     * Delete concrete entry
     * 
     * @Route("{id}/", requirements={"id" = "\d+"})
     * Method({"DELETE"})
     * @ParamConverter("id", class="ApiBundle:Company")
     * @param Company $company
     * @return JsonResponse
     */
    public function deleteAction(Company $company) {
        $em = $this->getDoctrine()->getManager();
        $em->remove($company);
        $em->flush();

        return new JsonResponse([], 204);
    }
    
    /**
     * @DI\LookupMethod("api_company_repository")
     * @return CompanyRepository
     *
    */
    public function getCompanyRepository(){
        
    }

}
