<?php

namespace ApiBundle\Controller;

use ApiBundle\Entity\StoragePlace;
use ApiBundle\Form\StoragePlaceForm;
use ApiBundle\Repository\StoragePlaceRepository;
use JMS\DiExtraBundle\Annotation as DI;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/storagePlaces/")
 * @author Norbert Jurkiewicz <norbert.jurkiewicz@gmail.com>
 */
class StoragePlaceController extends ApiMainController
{
    
    /**
     * Get all companies
     * 
     * @Route("")
     * @Method({"GET"})
     * @return JsonResponse
     */
    public function indexAction()
    {
        $data = [];
        $storagePlaces = $this->getStoragePlaceRepository()->findAll();
        foreach ($storagePlaces as $storagePlace) {
            $data[] = $this->getStoragePlaceRepository()->getData($storagePlace, 2);
        }

        return $this->getResponse($data);
    }

    /**
     * @Route("paginated/")
     * @Method({"GET"})
     * @param Request $request
     * @return JsonResponse
     */
    public function indexPaginatedAction(Request $request)
    {
        $data = [];

        $offset = $request->query->get('offset');


        if(is_null($offset))
        {
            $offset = 0;
        }

        $places = $this->getStoragePlaceRepository()->getAll(true,$offset);
        foreach ($places as $place) {
            $data[] = $this->getStoragePlaceRepository()->getData($place);
        }
        $data['TOTAL_AMOUNT'] = $this->getStoragePlaceRepository()->countAll();
        return $this->getResponse($data);
    }
    /**
     * Find books by title
     *
     * @Route("find/")
     * @Method({"GET"})
     * @param Request $request
     * @return JsonResponse
     */
    public function findAction(Request $request)
    {
        $offset = $request->query->get('offset');

        if(is_null($offset))
        {
            $offset = 0;
        }
        //@todo: validation empty
        $sSearchValue = $request->get('search_value');
        $sSearchField = $request->get('field');

        $data = [];
        $places = $this->getStoragePlaceRepository()->findStorage($sSearchValue, $sSearchField, $offset);
        foreach ($places['RESULTS'] as $place) {
            $data[] = $this->getStoragePlaceRepository()->getData($place);
        }

        $data['TOTAL_AMOUNT'] = $places['TOTAL_AMOUNT'];

        return $this->getResponse($data);
    }
    /**
     * Get one storagePlace
     * 
     * @Route("{id}/", requirements={"id": "\d+"})
     * @Method({"GET"})
     * @ParamConverter("id", class="ApiBundle:StoragePlace")
     * @param Request $request
     * @param StoragePlace $storagePlace
     * @return JsonResponse
     */
    public function getAction(StoragePlace $storagePlace){
        $data = $this->getStoragePlaceRepository()->getData($storagePlace, 2);
        
        return $this->getResponse($data);
    }
    
    /**
     * Add new storagePlace
     * 
     * @Route("")
     * @Method({"POST"})
     * @return JsonResponse
     */
    public function addAction(){
        $storagePlace = new StoragePlace();
        $form = $this->createForm(StoragePlaceForm::class, $storagePlace);
        $request = $this->getFormService()->prepareRequest($form);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($storagePlace);
            $em->flush();
            
            return new JsonResponse(['id' => $storagePlace->getId()], 201);
        } else {

            return $this->getFormService()->getErrorResponse($form);
        }
    }
    
    /**
     * Edit storagePlace
     * 
     * @Route("{id}/", requirements={"id": "\d+"})
     * @Method({"PUT"})
     * @ParamConverter("id", class="ApiBundle:StoragePlace")
     * @param Request $request
     * @param StoragePlace $storagePlace
     * @return JsonResponse
     */
    public function editAction(StoragePlace $storagePlace) {
        $form = $this->createForm(StoragePlaceForm::class, $storagePlace, ['method' => 'PUT']);
        $request = $this->getFormService()->prepareRequest($form);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->flush();

            return new JsonResponse(['id' => $storagePlace->getId()], 201);
        } else {

            return $this->getFormService()->getErrorResponse($form);
        }
    }

    /**
     * Delete concrete entry
     * 
     * @Route("{id}/", requirements={"id" = "\d+"})
     * Method({"DELETE"})
     * @ParamConverter("id", class="ApiBundle:StoragePlace")
     * @param StoragePlace $storagePlace
     * @return JsonResponse
     */
    public function deleteAction(StoragePlace $storagePlace) {
        $em = $this->getDoctrine()->getManager();
        $em->remove($storagePlace);
        $em->flush();

        return new JsonResponse([], 204);
    }
    
    /**
     * @DI\LookupMethod("api_storagePlace_repository")
     * @return StoragePlaceRepository
    */
    public function getStoragePlaceRepository(){
        
    }

}
