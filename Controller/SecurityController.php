<?php
namespace ApiBundle\Controller;

use ApiBundle\Form\LoginForm;
use JMS\DiExtraBundle\Annotation as DI;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

/**
 * @author Norbert Jurkiewicz <norbert.jurkiewicz@gmail.com>
 */
class SecurityController extends Controller {
    
    /**
     * At present login and token_login are common, keep in mind this fact
     * 
     * @Route("/login", name="login")
     * @Route("/token_login", name="token_login")
     * @Template()
     */
     public function loginAction(Request $request)
    {
        //traditional or api log in 
        if ($request->get('_route') === 'token_login') {
            $action = $this->generateUrl('token_login_check');
        } else {
            $action = $this->generateUrl('login_check');
        }
        $form = $this->createForm(LoginForm::class, null, ['action' => $action]);
        $authenticationUtils = $this->getAuthenticationUtils();
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();
        
        return[
            'last_username' => $lastUsername,
            'error' => $error,
            'form' => $form->createView()
        ];
    }   
    
    /**
     * @DI\LookupMethod("security.authentication_utils")
     * @return AuthenticationUtils
    */
    public function getAuthenticationUtils(){
        
    }
}
