<?php

namespace ApiBundle\Controller;

use ApiBundle\Entity\Inventory;
use ApiBundle\Form\InventoryForm;
use ApiBundle\Repository\InventoryRepository;
use JMS\DiExtraBundle\Annotation as DI;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use ApiBundle\Form\DateForm;

/**
 * @Route("/inventories/")
 * @author Norbert Jurkiewicz <norbert.jurkiewicz@gmail.com>
 */
class InventoryController extends ApiMainController
{
    
    /**
     * Get all inventorys
     * 
     * @Route("")
     * @Method({"GET"})
     * @return JsonResponse
     */
    public function indexAction()
    {
        $data = [];
        $inventorys = $this->getInventoryRepository()->getAll();
        foreach ($inventorys as $inventory) {
            $data[] = $this->getInventoryRepository()->getData($inventory, 2);
        }

        return $this->getResponse($data);
    }

    /**
     * @Route("paginated/")
     * @Method({"GET"})
     * @param Request $request
     * @return JsonResponse
     */
    public function indexPaginatedAction(Request $request)
    {
        $data = [];

        $offset = $request->query->get('offset');


        if(is_null($offset))
        {
            $offset = 0;
        }

        $inventories = $this->getInventoryRepository()->getAll(true,$offset);
        foreach ($inventories as $inventory) {
            $data[] = $this->getInventoryRepository()->getData($inventory, 2);
        }
        $data['TOTAL_AMOUNT'] = $this->getInventoryRepository()->countAll();
        return $this->getResponse($data);
    }

    /**
     * Find books by title
     *
     * @Route("find/")
     * @Method({"GET"})
     * @param Request $request
     * @return JsonResponse
     */
    public function findAction(Request $request)
    {
        $offset = $request->query->get('offset');

        if(is_null($offset))
        {
            $offset = 0;
        }
        //@todo: validation empty
        $sSearchValue = $request->get('search_value');
        $sSearchField = $request->get('field');

        $data = [];
        $inventories = $this->getInventoryRepository()->findInventory($sSearchValue, $sSearchField, $offset);
        foreach ($inventories['RESULTS'] as $inventory) {
            $data[] = $this->getInventoryRepository()->getData($inventory, 2);
        }

        $data['TOTAL_AMOUNT'] = $inventories['TOTAL_AMOUNT'];

        return $this->getResponse($data);
    }
    /**
     * Get one inventory
     * 
     * @Route("{id}/", requirements={"id": "\d+"})
     * @Method({"GET"})
     * @ParamConverter("id", class="ApiBundle:Inventory")
     * @param Request $request
     * @param Inventory $inventory
     * @return JsonResponse
     */
    public function getAction(Inventory $inventory){
        $data = $this->getInventoryRepository()->getData($inventory, 2);
        
        return $this->getResponse($data);
    }
    
    /**
     * Add new inventory
     * 
     * @Route("")
     * @Method({"POST"})
     * @return JsonResponse
     */
    public function addAction(){
        $inventory = new Inventory();
        $form = $this->createForm(InventoryForm::class, $inventory);
        $request = $this->getFormService()->prepareRequest($form);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($inventory);
            $em->flush();
            
            return new JsonResponse(['id' => $inventory->getId()], 201);
        } else {

            return $this->getFormService()->getErrorResponse($form);
        }
    }
    
    /**
     * Edit inventory
     * 
     * @Route("{id}/", requirements={"id": "\d+"})
     * @Method({"PUT"})
     * @ParamConverter("id", class="ApiBundle:Inventory")
     * @param Request $request
     * @param Inventory $inventory
     * @return JsonResponse
     */
    public function editAction(Inventory $inventory) {
        $form = $this->createForm(InventoryForm::class, $inventory, ['method' => 'PUT']);
        $request = $this->getFormService()->prepareRequest($form);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->flush();

            return new JsonResponse(['id' => $inventory->getId()], 201);
        } else {

            return $this->getFormService()->getErrorResponse($form);
        }
    }

    /**
     * Borrow inventory
     *
     * @Route("{id}/borrow/", requirements={"id": "\d+"})
     * @Method({"PUT"})
     * @ParamConverter("id", class="ApiBundle:Inventory")
     * @param Inventory $inventory
     * @param Request $request
     * @return JsonResponse
     */
    public function borrowAction(Inventory $inventory, Request $request)
    {
        if($inventory->getIsBorrowed())
        {
            return new JsonResponse(['id' => $inventory->getId()], 404);
        }
        //@Todo: validation


        $form = $this->createForm(InventoryForm::class, $inventory, ['method' => 'PUT']);
        $request = $this->getFormService()->prepareRequest($form);
        $form->handleRequest($request);

        if($form->isValid())
        {
            $inventory->setIsBorrowed(true);
            $inventory->setOwner($this->getUser());

            return new JsonResponse([$inventory], 401);
            $em = $this->getDoctrine()->getManager();
            $em->flush();
            return new JsonResponse(['id' => $inventory->getId()], 201);
        }

        return $this->getFormService()->getErrorResponse($form);

    }

    /**
     *
     * Return inventory
     *
     *
     * @Route("{id}/return/", requirements={"id": "\d+"})
     * @Method({"PUT"})
     * @ParamConverter("id", class="ApiBundle:Inventory")
     * @param Inventory $inventory
     * @return JsonResponse
     */
    public function returnAction(Inventory $inventory)
    {
        if(!$inventory->getIsBorrowed())
        {
            return new JsonResponse(['id' => $inventory->getId()], 404);
        }
        $inventory->setIsBorrowed(false);
        $inventory->setOwner(null);
        $inventory->setReturnDate(null);

        $em = $this->getDoctrine()->getManager();
        $em->flush();

        return new JsonResponse(['id' => $inventory->getId()], 201);
    }

    /**
     * Delete concrete product
     * @Route("{id}/", requirements={"id" = "\d+"})
     * Method({"DELETE"})
     * @ParamConverter("id", class="ApiBundle:Inventory")
     * @param Inventory $inventory
     * @return JsonResponse
     */
    public function deleteAction(Inventory $inventory) {
        $em = $this->getDoctrine()->getManager();
        $em->remove($inventory);
        $em->flush();

        return new JsonResponse([], 204);
    }
    
    /**
     * @DI\LookupMethod("api_inventory_repository")
     * @return InventoryRepository
    */
    public function getInventoryRepository(){
        
    }

}
