<?php
namespace ApiBundle\Service;

use ApiBundle\Entity\User as User2;
use ApiBundle\Entity\UserLogIn;
use ApiBundle\Repository\ClientRepository;
use ApiBundle\Repository\CustomerRepository;
use ApiBundle\Repository\UserRepository;
use Doctrine\Common\Util\Debug;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\User;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

/**
 * UserProvider
 *
 * @author Norbert Jurkiewicz <norbert.jurkiewicz@gmail.com>
 */
class UserProvider implements UserProviderInterface
{

    /**
     *
     * @var UserRepository
     */
    protected $userRepository;

    /**
     *
     * @var RequestStack
     */
    private $requestStack;

    /**
     *
     * @var EntityManager
     */
    private $entityManager;

    /**
     *
     * @var ClientRepository
     */
    protected $clientRepository;

    /**
     *
     * @param UserRepository $userRepository            
     * @param RequestStack $requestStack            
     * @param EntityManager $entityManager            
     */
    public function __construct(
            UserRepository $userRepository,
            RequestStack $requestStack,
            EntityManager $entityManager,
            ClientRepository $clientRepository
    ){
        $this->userRepository = $userRepository;
        $this->requestStack = $requestStack;
        $this->entityManager = $entityManager;
        $this->clientRepository = $clientRepository;
    }

    /**
     *
     * @param type $username            
     * @return type
     */
    public function loadUserByUsername($username)
    {
        $user = $this->userRepository->findOneBy([
            'email' => $username,
        ]);
        
        if (null == $user) {
            throw new UsernameNotFoundException();
        }
        
        return $user;
    }

    public function refreshUser(UserInterface $user)
    {
        $class = get_class($user);
        if (! $this->supportsClass($class)) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', $class));
        }
        
        return $this->userRepository->find($user->getId());
    }

    public function supportsClass($class)
    {
        return $class === 'ApiBundle\Entity\User';
    }

}
