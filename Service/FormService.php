<?php
namespace ApiBundle\Service;
//use OAuth2\OAuth2;


use Doctrine\Common\Util\Debug;
use Exception;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;

/**
 * Bag of useful form methods
 *
 * @author Norbert Jurkiewicz <norbert.jurkiewicz@gmail.com>
 */
class FormService {
    
    /**
     *
     * @var RequestStack
     */
    private $requestStack;
    
    public function __construct(RequestStack $requestStack) {
        $this->requestStack = $requestStack;
    }

    /**
     * Prepare request, it provides compatibility between API and Form
     * 
     * @param Form $form
     * @param array $replaceNames = [] Replace parameters names [oldName => newName]
     * @param array $additionalData = []
     * @return Request
     */
    public function prepareRequest(Form $form, $replaceNames = [], $additionalData = []) {
        $request = $this->requestStack->getCurrentRequest();
        $data = (array) \json_decode($request->getContent(), true);
        if ($this->isAssoc($data)) {
            //normal type
            if (!empty($replaceNames)) {
                $data = $this->replaceNames($replaceNames, $data);
            }
            $data = array_merge($data, $additionalData);
        } else {
            //collection type
            foreach ($data as $key => $value) {
                if (!empty($replaceNames)) {
                    $value = $this->replaceNames($replaceNames, $value);
                }
                $data[$form->getName()][$key] = array_merge($value, $additionalData);
                unset($data[$key]);
            }
        }
        
        $request->request->set($form->getName(), $data);
        
        return $request;
    }

    /**
     * Change names
     * 
     * @param type $replaceNames
     * @param type $data
     * @return type
     */
    private function replaceNames($replaceNames, $data){
        foreach($replaceNames as $oldName => $newName){
            if(isset($data[$oldName])){
                $data[$newName] = $data[$oldName];
                unset($data[$oldName]);
            }
        }
        
        return $data;
    }
    
    /**
     * Return all errors from form
     * @param Form $form
     * @return []
     */
    public function getErrors($form)     {
        static $i=0;
        $i++;
        if($i>1000){
            throw new Exception('Recursive limit exceeded');
        }
        $errors = [];
        if ($form instanceof Form) {
            foreach ($form->getErrors() as $error) {
                $errors[] = $error->getMessage();
            }
            foreach ($form->all() as $key => $child) {
                /** @var $child \Symfony\Component\Form\Form */
                if ($err = $this->getErrors($child)) {
                    $errors[$key] = $err;
                }
            }
        }

        return $errors;
    }
    
    /**
     * Return prepared JsonResponse with errors or exception when no errors
     * 
     * @param Form $form
     * @return JsonResponse|boolean
     */
    public function getErrorResponse($form){
        $errors = $this->getErrors($form);
        if (empty($errors)) {

            throw new Exception('Form isn\'t valid, but id doesn\'t contain any error');
        } else {
            $return['errors'] = $this->getErrors($form);
            //$return['error'] = OAuth2::ERROR_INVALID_REQUEST; //error string
            $response = new JsonResponse($return);
            $response->setStatusCode(Response::HTTP_BAD_REQUEST);

            return $response;
        }
    }
    
    /**
     * Check, if array is associative or sequential
     * @param array $arr
     * @return boolean
     */
    private function isAssoc(array $arr) {
        
        return array_keys($arr) !== range(0, count($arr) - 1);
    }

}
