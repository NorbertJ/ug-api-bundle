<?php
namespace ApiBundle\Repository;

use ApiBundle\Entity\UserRole;
use Doctrine\ORM\EntityRepository;

/**
 *
 * @author Norbert Jurkiewicz <norbert.jurkiewicz@gmail.com>
 */
class UserRoleRepository extends EntityRepository {

    /**
     * @param UserRole $userRole
     * @return array
     */
    public function getData(UserRole $userRole){
        $data = [
            'id' => $userRole->getId(),
            'name' => $userRole->getName(),
            'role' => $userRole->getRole(),
        ];

        return $data;
    }
}
