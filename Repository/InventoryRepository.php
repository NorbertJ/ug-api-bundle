<?php
namespace ApiBundle\Repository;

use ApiBundle\Entity\Inventory;
use Doctrine\ORM\EntityRepository;

/**
 *
 * @author Norbert Jurkiewicz <norbert.jurkiewicz@gmail.com>
 */
class InventoryRepository extends EntityRepository {

    const DEFAULT_BLOCKSIZE = 5;
    /**
     *
     * @var UserRepository
     */
    private $userRepository;
    
    /**
     *
     * @var InvoiceRepository
     */
    private $invoiceRepository;
    
    /**
     *
     * @var StoragePlaceRepository
     */
    private $storagePlaceRepository;
    
    /**
     * Get all inventorys
     *
     * @param $paginated
     * @param $offset
     * @return Inventory[]
     */
    public function getAll($paginated = false, $offset = 0) {
        $qb = $this->createQueryBuilder('a');
        $qb->select('a', 'b', 'c', 'd');
        $qb->groupBy('a.id');
        $qb->leftJoin('a.invoice', 'b');
        $qb->leftJoin('a.responsiblePerson', 'c');
        $qb->leftJoin('a.storagePlace', 'd');
        if($paginated)
        {
            $first = ($offset * self::DEFAULT_BLOCKSIZE);
            $qb->setMaxResults(self::DEFAULT_BLOCKSIZE);
            $qb->setFirstResult($first);
        }
        return $qb->getQuery()->getResult();
    }

    /**
     *
     * @param $user_id
     * @return array
     */
    public function getUsersInventories($user_id)
    {
        $aInventory = $this->findBy(array(
            'owner' => $user_id
        ));
        if(!is_null($aInventory))
        {
            return $aInventory;
        }
        return [];
    }

    public function countAll()
    {
        $qb = $this->createQueryBuilder('a');
        $qb->select('COUNT(a)');
        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * find inventory
     * @param $search_value
     * @param $column
     * @param $offset
     * @return Inventory []
     */
    public function findInventory($search_value, $column, $offset = 0)
    {
        $count = $query = $this->createQueryBuilder('a')
            ->where('a.'.$column.' LIKE :search_value')
            ->setParameter(
                'search_value', '%'.$search_value.'%'
            )
            ->select('count(a.id)')
            ->getQuery()
            ->getSingleScalarResult();
        $first = ($offset * self::DEFAULT_BLOCKSIZE);
        $query = $this->createQueryBuilder('a')
            ->where('a.'.$column.' LIKE :search_value')
            ->setParameter(
                'search_value', '%'.$search_value.'%'
            )
            ->setMaxResults(self::DEFAULT_BLOCKSIZE)
            ->setFirstResult($first);

        return [
            'TOTAL_AMOUNT' => $count,
            'RESULTS' => $query->getQuery()->getResult()
        ];
    }
    /**
     * @param Inventory $inventory
     * @param int $depth = 1
     * @return array
     */
    public function getData(Inventory $inventory, $depth = 1) {
        $invoice = [];
        $responsiblePerson = [];
        $storagePlace = [];
        if ($depth > 1) {
            if(!$inventory->getIsArchived())
            {
                $invoice = $this->invoiceRepository->getData($inventory->getInvoice(), ($depth - 1));
            }
            $responsiblePerson = $this->userRepository->getData($inventory->getResponsiblePerson(), ($depth - 1));
            $storagePlace = $this->storagePlaceRepository->getData($inventory->getStoragePlace(), ($depth - 1));
        }
        $owner = [];
        if(!is_null($inventory->getOwner()))
        {
            $owner[$inventory->getOwner()->getId()] = $this->userRepository->getFirstNameLastName($inventory->getOwner());
        }
        $data = [
            'id' => $inventory->getId(),
            'isArchived' => $inventory->getIsArchived(),
            'value' => $inventory->getValue(),
            'type' => $inventory->getType(),
            'serialNumber' => $inventory->getSerialNumber(),
            'productionDate' => $inventory->getProductionDate(),
            'inventoryNumber' => $inventory->getInventoryNumber(),
            'barCode' => $inventory->getBarCode(),
            'usePlace' => $inventory->getUsePlace(),
            'aparatureOrMaterial' => $inventory->getAparatureOrMaterial(),
            'invoice' => $invoice,
            'responsiblePerson' => $responsiblePerson,
            'storagePlace' => $storagePlace,
            'owner' => $owner,
            'returnDate' => $inventory->getReturnDate(),
            'isBorrowed' => $inventory->getIsBorrowed()
        ];

        return $data;
    }

    /**
     * 
     * @param UserRepository $userRepository
     */
    public function setUserRepository(UserRepository $userRepository){
        
        $this->userRepository = $userRepository;
    }
    
    /**
     * 
     * @param InvoiceRepository $invoiceRepository
     */
    public function setInvoiceRepository(InvoiceRepository $invoiceRepository){
        
        $this->invoiceRepository = $invoiceRepository;
    }
    /**
     * 
     * @param StoragePlaceRepository $storagePlaceRepository
     */
    public function setStoragePlaceRepository(StoragePlaceRepository $storagePlaceRepository){
        
        $this->storagePlaceRepository = $storagePlaceRepository;
    }
}
