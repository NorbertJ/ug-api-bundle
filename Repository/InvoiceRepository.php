<?php
namespace ApiBundle\Repository;

use ApiBundle\Entity\Invoice;
use Doctrine\ORM\EntityRepository;

/**
 *
 * @author Norbert Jurkiewicz <norbert.jurkiewicz@gmail.com>
 */
class InvoiceRepository extends EntityRepository {

    const DEFAULT_BLOCKSIZE = 5;

    /**
     *
     * @var GrantPaymentSourceRepository
     * @todo Change it into PaymentSourceRepository when other king of payment source appears
     */
    private $grantPaymentSourceRepository;
    
    /**
     *
     * @var CompanyRepository
     */
    private $companyRepository;
    
    /**
     * Get all books
     * 
     * @return Invoice[]
     */
    public function getAll($paginated = false, $offset = 0) {
        $qb = $this->createQueryBuilder('a');
        $qb->select('a', 'b', 'c');
        $qb->groupBy('a.id');

        $qb->leftJoin('a.paymentSources', 'b');
        $qb->leftJoin('a.company', 'c');
        if($paginated)
        {
            $first = ($offset * self::DEFAULT_BLOCKSIZE);
            $qb->setMaxResults(self::DEFAULT_BLOCKSIZE);
            $qb->setFirstResult($first);
        }
        return $qb->getQuery()->getResult();
    }

    public function countAll()
    {
        $qb = $this->createQueryBuilder('a');
        $qb->select('COUNT(a)');
        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * find invoices
     * @param $search_value
     * @param $column
     * @param $offset
     * @return Invoice []
     */
    public function findInvoice($search_value, $column, $offset = 0)
    {
        $count = $query = $this->createQueryBuilder('a')
            ->where('a.'.$column.' LIKE :search_value')
            ->setParameter(
                'search_value', '%'.$search_value.'%'
            )
            ->select('count(a.id)')
            ->getQuery()
            ->getSingleScalarResult();
        $first = ($offset * self::DEFAULT_BLOCKSIZE);
        $query = $this->createQueryBuilder('a')
            ->where('a.'.$column.' LIKE :search_value')
            ->setParameter(
                'search_value', '%'.$search_value.'%'
            )
            ->setMaxResults(self::DEFAULT_BLOCKSIZE)
            ->setFirstResult($first);

        return [
            'TOTAL_AMOUNT' => $count,
            'RESULTS' => $query->getQuery()->getResult()
        ];
    }
    /**
     * @param Invoice $invoice
     * @param int $depth = 1
     * @return array
     */
    public function getData(Invoice $invoice, $depth = 1) {
        $paymentSources = [];
        $company = null;
        if ($depth > 1) {
            foreach ($invoice->getPaymentSources() as $paymentSource) {
                $paymentSources[] = $this->grantPaymentSourceRepository->getData($paymentSource, ($depth - 1));
            }
            $company = $this->companyRepository->getData($invoice->getCompany());
        }
        $data = [
            'id' => $invoice->getId(),
            'number' => $invoice->getNumber(),
            'category' => $invoice->getCategory(),
            'date' => $invoice->getDate(),
            'description' => $invoice->getDescription(),
            'grossValue' => $invoice->getGrossValue(),
            'material' => $invoice->getMaterial(),
            'company' => $company,
            'paymentSources' => $paymentSources,
        ];

        return $data;
    }

    /**
     * @param GrantPaymentSourceRepository $grantPaymentSourceRepository
     */
    public function setGrantPaymentSourceRepository(GrantPaymentSourceRepository $grantPaymentSourceRepository){
        
        $this->grantPaymentSourceRepository = $grantPaymentSourceRepository;
    }
    
    /**
     * @param CompanyRepository $companyRepository
     */
    public function setCompanyRepository(CompanyRepository $companyRepository){
        
        $this->companyRepository = $companyRepository;
    }
}
