<?php
namespace ApiBundle\Repository;

use ApiBundle\Entity\Book;
use ApiBundle\Entity\PaymentSource;
use Doctrine\ORM\EntityRepository;

/**
 *
 * @author Norbert Jurkiewicz <norbert.jurkiewicz@gmail.com>
 */
class PaymentSourceRepository extends EntityRepository {
    
    /**
     * Data common for all payment sources
     * 
     * @param PaymentSource $paymentSource
     * @return array
     */    
    public function getCommonData(PaymentSource $paymentSource){
        $data = [
            'id' => $paymentSource->getId(),
            'name' => $paymentSource->getName(),
            'totalValue' => $paymentSource->getTotalValue(),
            'currentValue' => $paymentSource->getCurrentValue(),
            'isGrant' => $paymentSource->getIsGrant(),
        ];
        
        return $data;
    }

}
