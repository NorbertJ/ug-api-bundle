<?php
namespace ApiBundle\Repository;

use ApiBundle\Entity\User;
use Doctrine\ORM\EntityRepository;
use Exception;

/**
 *
 * @author Norbert Jurkiewicz <norbert.jurkiewicz@gmail.com>
 */
class UserRepository extends EntityRepository
{

    const RECURRENCY_LIMIT = 1000;
    const DEFAULT_BLOCKSIZE = 5;

    /**
     *
     * @var UserRoleRepository
     */
    private $userRoleRepository;

    /**
     * @var StudiesTypeRepository
     */
    private $studiesTypeRepository;

    /**
     * Get all users
     * @param $paginated
     * @param $offset
     * @return User[]
     */
    public function getAll($paginated = false, $offset = 0)
    {


        $qb = $this->createQueryBuilder('a');
        $qb->select('a');
        $qb->groupBy('a.id');
        if ($paginated) {
            $first = ($offset * self::DEFAULT_BLOCKSIZE);
            $qb->setMaxResults(self::DEFAULT_BLOCKSIZE);
            $qb->setFirstResult($first);
        }
        $qb->leftJoin('a.userRoles', 'b');
        $qb->leftJoin('a.promotors', 'c');

        return $qb->getQuery()->getResult();
    }

    public function countAll()
    {
        $qb = $this->createQueryBuilder('a');
        $qb->select('COUNT(a)');
        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * Get all authors
     *
     * @return User[]
     */
    public function getAllAuthors()
    {

        $qb = $this->createQueryBuilder('a');
        $qb->join('a.userRoles', 'b');
        $qb->andWhere('b.role = :rolePerson');
        $qb->setParameter('rolePerson', 'ROLE_PERSON');

        return $qb->getQuery()->getResult();
    }

    /**
     * @return User []
     */
    public function getAllWorkers()
    {
        $qb = $this->createQueryBuilder('a');
        $qb->join('a.userRoles', 'b');
        $qb->andWhere('b.role = :rolePerson');
        $qb->setParameter('rolePerson', 'ROLE_WORKER');

        return $qb->getQuery()->getResult();
    }

    /**
     * @param User $user
     * @return int
     */
    public function getUserId(User $user)
    {
        return $user->getId();
    }

    /**
     * @param User $user
     * @return string
     */
    public function getFirstNameLastName(User $user)
    {
        return $user->getFirstName() . ' ' . $user->getLastName();
    }

    /**
     * @param User $user
     * @param int $depth = 1
     * @return array
     */
    public function getData(User $user, $depth = 1)
    {
        //standard safety valve
        static $itterations = 0;
        if ($itterations > self::RECURRENCY_LIMIT) {

            throw new Exception('Recurrency limit has been exceeded');
        }
        $roles = [];

        foreach ($user->getRoles() as $role) {
            $roles[] = $this->userRoleRepository->getData($role);
        }

        $promotors = [];
        if ($depth > 1) {

            if ($user->getPromotors()->count() > 0) {
                foreach ($user->getPromotors() as $promotor) {
                    $promotors[$promotor->getId()] = $this->getFirstNameLastName($promotor);
                }
            }
        }
        $data = $this->getStandardData($user);

        $data = $this->setRolesInfo($data, $roles);

        $data['promotors'] = $promotors;

        return $data;
    }

    /**
     * find users
     * @param $search_value
     * @param $column
     * @param $offset
     * @return User []
     */
    public function findUser($search_value, $column, $offset = 0)
    {
        $first = ($offset * self::DEFAULT_BLOCKSIZE);

        $count = $query = $this->createQueryBuilder('a')
            ->where('a.' . $column . ' LIKE :search_value')
            ->setParameter(
                'search_value', '%' . $search_value . '%'
            )
            ->select('count(a.id)')
            ->getQuery()
            ->getSingleScalarResult();

        $query = $this->createQueryBuilder('a')
            ->where('a.' . $column . ' LIKE :search_value')
            ->setParameter(
                'search_value', '%' . $search_value . '%'
            )
            ->setMaxResults(self::DEFAULT_BLOCKSIZE)
            ->setFirstResult($first);;

        return [
            'TOTAL_AMOUNT' => $count,
            'RESULTS' => $query->getQuery()->getResult()
        ];
    }

    /**
     * @param $email
     * @return array
     */
    public function getUserByEmail($email)
    {
        $User = $this->findOneByEmail($email);
        return $this->getData($User, 2);
    }

    /**
     * @param $aData
     * @param $aRoles
     * @return $aData with roles set
     */
    private function setRolesInfo($aData, $aRoles)
    {
        if (empty($aRoles)) {
            return $aData;
        }
        foreach ($aRoles as $role) {
            if ($role['role'] == 'ROLE_PERSON') {
                $aData['isAuthor'] = true;
            }
            if ($role['role'] == 'ROLE_GUEST') {
                $aData['isGuest'] = true;
            }
            if ($role['role'] == 'ROLE_USER') {
                $aData['isUser'] = true;
            }
            if ($role['role'] == 'ROLE_WORKER') {
                $aData['isWorker'] = true;
            }
            if ($role['role'] == 'ROLE_MASTER') {
                $aData['isMaster'] = true;
            }
            if ($role['role'] == 'ROLE_LICENTIATE') {
                $aData['isLicenciate'] = true;
            }
            if ($role['role'] == 'ROLE_ADMIN' || $role['role'] == 'ROLE_OWNER') {
                $aData['isAdmin'] = true;
            }

        }
        return $aData;
    }

    /**
     * @param User $user
     * @return array
     */
    private function getStandardData(User $user)
    {
        $sType = '';

        if(!is_null($user->getTypeOfStudies())){
            $sType = $user->getTypeOfStudies()->getName();
        }

        $data = [
            'id' => $user->getId(),
            'email' => $user->getEmail(),
            'isActive' => $user->getIsActive(),
            'firstName' => $user->getFirstName(),
            'lastName' => $user->getLastName(),
            'pesel' => $user->getPesel(),
            'birthDate' => $this->getDateShortFormatted($user->getBirthDate()),
            'workerNumber' => $user->getWorkerNumber(),
            'workingSinceDate' => $this->getDateShortFormatted($user->getWorkingSinceDate()),
            'physicianTestsValidDate' => $this->getDateShortFormatted($user->getPhysicianTestsValidDate()),
            'position' => $user->getPosition(),
            'address' => $user->getAddress(),
            'isConsulting' => $user->getIsConsulting(),
            'consultationDate' => $user->getConsultationDate(),
            'bankAccountNumber' => $user->getBankAccountNumber(),
            'phoneNumber' => $user->getPhoneNumber(),
            'roomNumber' => $user->getRoomNumber(),
            'typeOfStudies' => $sType,
            'fieldOfStudies' => $user->getFieldOfStudies(),
            'lab' => $user->getLab(),
            'title' => $user->getTitle(),
            'isGuest' => false,
            'isUser' => false,
            'isWorker' => false,
            'isMaster' => false,
            'isLicenciate' => false,
            'isAdmin' => false,
            'isAuthor' => false
        ];

        return $data;
    }



    /**
     * Returns date in format 'd-m-y'
     * @param $oDate
     * @return null
     */
    private function getDateShortFormatted($oDate)
    {
        if (is_null($oDate)) {
            return null;
        }

        return $oDate->format('d-m-Y');
    }

    /**
     *
     * @param UserRoleRepository $userRoleRepository
     */
    public function setUserRoleRepository(UserRoleRepository $userRoleRepository)
    {

        $this->userRoleRepository = $userRoleRepository;
    }
}