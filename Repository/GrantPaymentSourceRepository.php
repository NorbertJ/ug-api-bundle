<?php
namespace ApiBundle\Repository;

use ApiBundle\Entity\GrantPaymentSource;

/**
 *
 * @author Norbert Jurkiewicz <norbert.jurkiewicz@gmail.com>
 */
class GrantPaymentSourceRepository extends PaymentSourceRepository {

    const DEFAULT_BLOCKSIZE = 5;

    /**
     *
     * @var UserRepository
     */
    private $userRepository;
   
    /**
     * Get all books
     *
     * @param $offset
     * @param $paginated
     * @param $withDs
     * 
     * @return GrantPaymentSource[]
     */
    public function getAll($paginated = false, $offset = 0, $withDs = false) {
        $qb = $this->createQueryBuilder('a');
        $qb->select('a', 'b', 'c');
        $qb->groupBy('a.id');
        if(!$withDs)
        {
            $qb->where('a.isDs = false');
            $qb->where('a.isDs IS NULL');
        }
        if($withDs)
        {
            $qb->where('a.isDs = true');
        }
        $qb->leftJoin('a.contractors', 'b');
        $qb->leftJoin('a.manager', 'c');
        if($paginated)
        {
            $first = ($offset * self::DEFAULT_BLOCKSIZE);
            $qb->setMaxResults(self::DEFAULT_BLOCKSIZE);
            $qb->setFirstResult($first);
        }

        return $qb->getQuery()->getResult();
    }

    public function countAll($withDs = false)
    {
        $qb = $this->createQueryBuilder('a');
        if(!$withDs)
        {
            $qb->where('a.isDs = false');
            $qb->where('a.isDs IS NULL');
        }
        if($withDs)
        {
            $qb->where('a.isDs = true');
        }
        $qb->select('COUNT(a)');
        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * find grant payment
     * @param $search_value
     * @param $column
     * @param $offset
     * @return GrantPaymentSource []
     */
    public function findGrant($search_value, $column, $offset = 0)
    {
        $count = $query = $this->createQueryBuilder('a')
            ->where('a.'.$column.' LIKE :search_value')
            ->setParameter(
                'search_value', '%'.$search_value.'%'
            )
            ->select('count(a.id)')
            ->getQuery()
            ->getSingleScalarResult();
        $first = ($offset * self::DEFAULT_BLOCKSIZE);
        $query = $this->createQueryBuilder('a')
            ->where('a.'.$column.' LIKE :search_value')
            ->setParameter(
                'search_value', '%'.$search_value.'%'
            )
            ->setMaxResults(self::DEFAULT_BLOCKSIZE)
            ->setFirstResult($first);

        return [
            'TOTAL_AMOUNT' => $count,
            'RESULTS' => $query->getQuery()->getResult()
        ];
    }
    /**
     * @param GrantPaymentSource $grantPaymentSource
     * @param int $depth = 1
     * @return array
     */
    public function getData(GrantPaymentSource $grantPaymentSource, $depth = 1) {
        $contractors = [];
        $manager = null;
        if ($depth > 1) {
            foreach ($grantPaymentSource->getContractors() as $contractor) {
                $contractors[] = $this->userRepository->getData($contractor, ($depth - 1));
            }
            $manager = $this->userRepository->getData($grantPaymentSource->getManager(), ($depth - 1));
        }
        $commonData = $this->getCommonData($grantPaymentSource);
        $data = $commonData + [
            'description' => $grantPaymentSource->getDescription(),
            'fromDate' => $grantPaymentSource->getFromDate(),
            'number' => $grantPaymentSource->getNumber(),
            'source' => $grantPaymentSource->getSource(),
            'title' => $grantPaymentSource->getTitle(),
            'toDate' => $grantPaymentSource->getToDate(),
            'manager' => $manager,
            'contractors' => $contractors,
                'isDs' => $grantPaymentSource->getIsDs(),
                'year' => $grantPaymentSource->getYear()
        ];

        return $data;
    }

    /**
     * 
     * @param UserRepository $userRepository
     */
    public function setUserRepository(UserRepository $userRepository){
        
        $this->userRepository = $userRepository;
    }
}
