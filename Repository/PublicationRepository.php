<?php
namespace ApiBundle\Repository;

use ApiBundle\Entity\Publication;
use Doctrine\ORM\EntityRepository;

/**
 *
 * @author Norbert Jurkiewicz <norbert.jurkiewicz@gmail.com>
 */
class PublicationRepository extends EntityRepository {

    const DEFAULT_BLOCKSIZE = 5;
    /**
     *
     * @var UserRepository
     */
    private $userRepository;
    
    /**
     * Get all books
     * @param $offset
     * @param $paginated
     * @return Publication[]
     */
    public function getAll($paginated = false, $offset = 0) {
        $qb = $this->createQueryBuilder('a');
        $qb->select('a', 'b');
        $qb->groupBy('a.id');
        $qb->leftJoin('a.authors', 'b');
        if($paginated)
        {
            $first = ($offset * self::DEFAULT_BLOCKSIZE);
            $qb->setMaxResults(self::DEFAULT_BLOCKSIZE);
            $qb->setFirstResult($first);
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * find books
     * @param $search_value
     * @param $column
     * @param $offset
     * @return Publication []
     */
    public function findPublication($search_value, $column, $offset = 0)
    {

        $count = $query = $this->createQueryBuilder('a')
            ->where('a.'.$column.' LIKE :search_value')
            ->setParameter(
                'search_value', '%'.$search_value.'%'
            )
            ->select('count(a.id)')
            ->getQuery()
            ->getSingleScalarResult();
        $first = ($offset * self::DEFAULT_BLOCKSIZE);
        $query = $this->createQueryBuilder('a')
            ->where('a.'.$column.' LIKE :search_value')
            ->setParameter(
                'search_value', '%'.$search_value.'%'
            )
            ->setMaxResults(self::DEFAULT_BLOCKSIZE)
            ->setFirstResult($first);

        return [
            'TOTAL_AMOUNT' => $count,
            'RESULTS' => $query->getQuery()->getResult()
        ];
    }

    /**
     * @return mixed
     */
    public function countAll()
    {
        $qb = $this->createQueryBuilder('a');
        $qb->select('COUNT(a)');
        return $qb->getQuery()->getSingleScalarResult();
    }
    
    /**
     * @param Publication $publication
     * @param int $depth = 1
     * @return array
     */
    public function getData(Publication $publication, $depth = 1) {
        $authors = [];
        if ($depth > 1) {
            foreach ($publication->getAuthors() as $author) {
                $authors[$author->getId()] = $this->userRepository->getFirstNameLastName($author);
            }
        }    
        $data = [
            'id' => $publication->getId(),
            'magazine' => $publication->getMagazine(),
            'pageNumber' => $publication->getPageNumber(),
            'pdf' => $publication->getPdf(),
            'points' => $publication->getPoints(),
            'title' => $publication->getTitle(),
            'year' => $publication->getYear(),
            'authors' => $authors,
        ];

        return $data;
    }

    /**
     * 
     * @param UserRepository $userRepository
     */
    public function setUserRepository(UserRepository $userRepository){
        
        $this->userRepository = $userRepository;
    }
}
