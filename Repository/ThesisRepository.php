<?php
namespace ApiBundle\Repository;

use ApiBundle\Entity\Thesis;
use Doctrine\ORM\EntityRepository;

/**
 *
 * @author Norbert Jurkiewicz <norbert.jurkiewicz@gmail.com>
 */
class ThesisRepository extends EntityRepository {

    const DEFAULT_BLOCKSIZE = 5;

    /**
     *
     * @var UserRepository
     */
    private $userRepository;
    
    /**
     * Get all books
     *
     * @param $paginated
     * @param $offset
     * @return Thesis[]
     */
    public function getAll($paginated = false, $offset = 0) {
        $qb = $this->createQueryBuilder('a');
        $qb->groupBy('a.id');
        $qb->select('a', 'b', 'c');
        $qb->leftJoin('a.authors', 'b');
        $qb->leftJoin('a.promotors', 'c');
        if($paginated)
        {
            $first = ($offset * self::DEFAULT_BLOCKSIZE);
            $qb->setMaxResults(self::DEFAULT_BLOCKSIZE);
            $qb->setFirstResult($first);
        }

        return $qb->getQuery()->getResult();
    }
    /**
     * find thesis
     * @param $search_value
     * @param $column
     * @param $offset
     * @return Thesis []
     */
    public function findThesis($search_value, $column, $offset = 0)
    {
        $count = $query = $this->createQueryBuilder('a')
            ->where('a.'.$column.' LIKE :search_value')
            ->setParameter(
                'search_value', '%'.$search_value.'%'
            )
            ->select('count(a.id)')
            ->getQuery()
            ->getSingleScalarResult();
        $first = ($offset * self::DEFAULT_BLOCKSIZE);
        $query = $this->createQueryBuilder('a')
            ->where('a.'.$column.' LIKE :search_value')
            ->setParameter(
                'search_value', '%'.$search_value.'%'
            )
            ->setMaxResults(self::DEFAULT_BLOCKSIZE)
            ->setFirstResult($first);

        return [
            'TOTAL_AMOUNT' => $count,
            'RESULTS' => $query->getQuery()->getResult()
        ];
    }

    public function countAll()
    {
        $qb = $this->createQueryBuilder('a');
        $qb->select('COUNT(a)');
        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * @param Thesis $thesis
     * @param int $depth = 1
     * @return array
     */
    public function getData(Thesis $thesis, $depth = 1) {
        $authors = [];
        $promotors = [];
        if ($depth > 1) {
            foreach ($thesis->getAuthors() as $author) {
                $authors[$author->getId()] = $this->userRepository->getFirstNameLastName($author);
            }
            foreach ($thesis->getPromotors() as $promotor) {
                $promotors[$promotor->getId()] = $this->userRepository->getFirstNameLastName($promotor);
            }
        }
        $data = [
            'id' => $thesis->getId(),
            'bookstand' => $thesis->getBookstand(),
            'degree' => $thesis->getDegree(),
            'group' => $thesis->getGroup(),
            'place' => $thesis->getPlace(),
            'shelf' => $thesis->getShelf(),
            'specie' => $thesis->getSpecie(),
            'title' => $thesis->getTitle(),
            'year' => $thesis->getYear(),
            'authors' => $authors,
            'promotors' => $promotors,
        ];

        return $data;
    }

    /**
     * 
     * @param UserRepository $userRepository
     */
    public function setUserRepository(UserRepository $userRepository){
        
        $this->userRepository = $userRepository;
    }
}
