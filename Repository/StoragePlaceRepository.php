<?php
namespace ApiBundle\Repository;

use ApiBundle\Entity\StoragePlace;
use Doctrine\ORM\EntityRepository;

/**
 *
 * @author Norbert Jurkiewicz <norbert.jurkiewicz@gmail.com>
 */
class StoragePlaceRepository extends EntityRepository {

    const DEFAULT_BLOCKSIZE = 5;
    /**
     * Get all Storage places
     *
     * @param $offset
     * @param $paginated
     * @return StoragePlace[]
     */
    public function getAll($paginated = false,$offset = 0) {

        $qb = $this->createQueryBuilder('a');
        if($paginated)
        {
            $first = ($offset * self::DEFAULT_BLOCKSIZE);
            $qb->setMaxResults(self::DEFAULT_BLOCKSIZE);
            $qb->setFirstResult($first);
        }

        return $qb->getQuery()->getResult();
    }

    public function countAll()
    {
        $qb = $this->createQueryBuilder('a');
        $qb->select('COUNT(a)');
        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * find books
     * @param $search_value
     * @param $column
     * @param $offset
     * @return StoragePlace []
     */
    public function findStorage($search_value, $column, $offset = 0)
    {
        $count = $query = $this->createQueryBuilder('a')
            ->where('a.'.$column.' LIKE :search_value')
            ->setParameter(
                'search_value', '%'.$search_value.'%'
            )
            ->select('count(a.id)')
            ->getQuery()
            ->getSingleScalarResult();
        $first = ($offset * self::DEFAULT_BLOCKSIZE);
        $query = $this->createQueryBuilder('a')
            ->where('a.'.$column.' LIKE :search_value')
            ->setParameter(
                'search_value', '%'.$search_value.'%'
            )
            ->setMaxResults(self::DEFAULT_BLOCKSIZE)
            ->setFirstResult($first);

        return [
            'TOTAL_AMOUNT' => $count,
            'RESULTS' => $query->getQuery()->getResult()
        ];
    }
    /**
     * @param StoragePlace $storagePlace
     * @return array
     */
    public function getData(StoragePlace $storagePlace) {
        $data = [
            'id' => $storagePlace->getId(),
            'name' => $storagePlace->getName(),
        ];

        return $data;
    }

}
