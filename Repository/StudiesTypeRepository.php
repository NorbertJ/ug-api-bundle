<?php
namespace ApiBundle\Repository;

use ApiBundle\Entity\StudiesType;
use Doctrine\ORM\EntityRepository;

/**
 *
 * @author Norbert Jurkiewicz <norbert.jurkiewicz@gmail.com>
 */
class StudiesTypeRepository extends EntityRepository {
    
    public function getName(StudiesType $studiesType)
    {
        return $studiesType->getName();
    }

}
