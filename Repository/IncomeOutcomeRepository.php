<?php
/**
 * File Description
 *
 * Some comprehensive Description of the files contents
 *
 * @author Norbert Jurkiewicz <norbert.jurkiewicz@interactivedata.com>
 * @user norbert
 * @version $Author:$ $Id:$ $Revision:$ $Date:$
 * @since 26.09.2016
 */

namespace ApiBundle\Repository;


use ApiBundle\Entity\GrantPaymentSource;
use ApiBundle\Entity\IncomeOutcome;
use ApiBundle\Entity\Invoice;
use Doctrine\ORM\EntityRepository;


class IncomeOutcomeRepository extends EntityRepository
{
    /**
     *
     * @var GrantPaymentSourceRepository
     */
    private $GrantPaymentSourceRepository;

    /**
     * @var InvoiceRepository
     */
    private $invoiceRepository;

    /**
     * @return array
     */
    public function getAll()
    {
        $qb = $this->createQueryBuilder('a');
        $qb->select('a', 'b', 'c');
        $qb->leftJoin('a.grantpaymentsource', 'b');
        $qb->leftJoin('a.invoice', 'c');
        $qb->groupBy('a.id');

        return $qb->getQuery()->getResult();
    }

    /**
     * @param Invoice $invoice
     * @return array
     */
    public function getForInvoice(Invoice $invoice)
    {
        $qb = $this->createQueryBuilder('a');
        $qb->select('a', 'b', 'c');
        $qb->where('a.invoice = :id');
        $qb->setParameter('id', $invoice->getId());
        $qb->leftJoin('a.grantpaymentsource', 'b');
        $qb->leftJoin('a.invoice', 'c');
        $qb->groupBy('a.id');
        return $qb->getQuery()->getResult();
    }

    /**
     * @param GrantPaymentSource $grantPaymentSource
     * @return array
     */
    public function getForPayment(GrantPaymentSource $grantPaymentSource)
    {
        $qb = $this->createQueryBuilder('a');
        $qb->select('a', 'b', 'c');
        $qb->where('a.grantpaymentsource = :id');
        $qb->setParameter('id', $grantPaymentSource->getId());
        $qb->leftJoin('a.grantpaymentsource', 'b');
        $qb->leftJoin('a.invoice', 'c');
        $qb->groupBy('a.id');
        return $qb->getQuery()->getResult();
    }

    /**
     * @param IncomeOutcome $incomeOutcome
     * @return array
     */
    public function getData(IncomeOutcome $incomeOutcome)
    {
        $aGrant = [];
        $aInvoice = [];
        if(!is_null($incomeOutcome->getGrantpaymentsource()))
        {
            $aGrant[] = $this->GrantPaymentSourceRepository->getData($incomeOutcome->getGrantpaymentsource(), 1);
        }

        if(!is_null($incomeOutcome->getInvoice()))
        {
            $aInvoice[] = $this->invoiceRepository->getData($incomeOutcome->getInvoice(), 1);
        }
        $data = [
            'id' => $incomeOutcome->getId(),
            'paymentSource' => $aGrant,
            'invoice' => $aInvoice,
            'value' => $incomeOutcome->getValue(),
            'description' => $incomeOutcome->getDescription()
        ];

        return $data;
    }


    /**
     * @param GrantPaymentSourceRepository $GrantPaymentSourceRepository
     */
    public function setGrantPaymentSourceRepository(GrantPaymentSourceRepository $GrantPaymentSourceRepository){

        $this->GrantPaymentSourceRepository = $GrantPaymentSourceRepository;
    }

    /**
     * @param InvoiceRepository $invoiceRepository
     */
    public function setInvoiceRepository(InvoiceRepository $invoiceRepository)
    {
        $this->invoiceRepository = $invoiceRepository;
    }
}