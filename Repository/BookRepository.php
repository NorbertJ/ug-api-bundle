<?php
namespace ApiBundle\Repository;

use ApiBundle\Entity\Book;
use Doctrine\ORM\EntityRepository;

/**
 *
 * @author Norbert Jurkiewicz <norbert.jurkiewicz@gmail.com>
 */
class BookRepository extends EntityRepository {

    const DEFAULT_BLOCKSIZE = 5;

    /**
     *
     * @var UserRepository
     */
    private $userRepository;
    
    /**
     * Get all books
     *
     * @param $offset
     * @param $paginated
     * @return Book[]
     */
    public function getAll($paginated = false,$offset = 0) {

        $qb = $this->createQueryBuilder('a');

        $qb->select('a', 'b');
        $qb->groupBy('a.id');
        $qb->leftJoin('a.authors', 'b');
        if($paginated)
        {
            $first = ($offset * self::DEFAULT_BLOCKSIZE);
            $qb->setMaxResults(self::DEFAULT_BLOCKSIZE);
            $qb->setFirstResult($first);
        }

        return $qb->getQuery()->getResult();
    }

    public function countAll()
    {
        $qb = $this->createQueryBuilder('a');
        $qb->select('COUNT(a)');
        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * find books
     * @param $search_value
     * @param $column
     * @param $offset
     * @return Book []
     */
    public function findBook($search_value, $column, $offset = 0)
    {
        $count = $query = $this->createQueryBuilder('a')
            ->where('a.'.$column.' LIKE :search_value')
            ->setParameter(
                'search_value', '%'.$search_value.'%'
            )
            ->select('count(a.id)')
            ->getQuery()
            ->getSingleScalarResult();
        $first = ($offset * self::DEFAULT_BLOCKSIZE);
        $query = $this->createQueryBuilder('a')
            ->where('a.'.$column.' LIKE :search_value')
            ->setParameter(
                'search_value', '%'.$search_value.'%'
            )
        ->setMaxResults(self::DEFAULT_BLOCKSIZE)
        ->setFirstResult($first);

        return [
            'TOTAL_AMOUNT' => $count,
            'RESULTS' => $query->getQuery()->getResult()
            ];
    }

    /**
     *
     * @param $user_id
     * @return array
     */
    public function getUsersBooks($user_id)
    {
        $aBooks = $this->findBy(array(
            'owner' => $user_id
        ));
        if(!is_null($aBooks))
        {
            return $aBooks;
        }
        return [];
    }
    /**
     * @param Book $book
     * @param int $depth = 1
     * @return array
     */
    public function getData(Book $book, $depth = 1) {
        $authors = [];
        if ($depth > 1) {
            foreach ($book->getAuthors() as $author) {
                $authors[$author->getId()] = $this->userRepository->getFirstNameLastName($author);
            }
        }
        $owner = [];
        if(!is_null($book->getOwner()))
        {
            $owner[$book->getOwner()->getId()] = $this->userRepository->getFirstNameLastName($book->getOwner());
        }
        $data = [
            'id' => $book->getId(),
            'bookstand' => $book->getBookstand(),
            'isbn' => $book->getIsbn(),
            'publisher' => $book->getPublisher(),
            'shelf' => $book->getShelf(),
            'title' => $book->getTitle(),
            'year' => $book->getYear(),
            'authors' => $authors,
            'isBorrowed' => $book->getIsBorrowed(),
            'owner' => $owner,
            'returnDate' => $book->getReturnDate(),
            'barCode' => $book->getBarCode()
        ];

        return $data;
    }


    /**
     * 
     * @param UserRepository $userRepository
     */
    public function setUserRepository(UserRepository $userRepository){
        
        $this->userRepository = $userRepository;
    }
}
