<?php
namespace ApiBundle\Repository;

use ApiBundle\Entity\Company;
use Doctrine\ORM\EntityRepository;

/**
 *
 * @author Norbert Jurkiewicz <norbert.jurkiewicz@gmail.com>
 */
class CompanyRepository extends EntityRepository {

    const DEFAULT_BLOCKSIZE = 5;
    /**
     * Get all companies
     *
     * @param $offset
     * @param $paginated
     * @return Company[]
     */
    public function getAll($paginated = false,$offset = 0) {

        $qb = $this->createQueryBuilder('a');

        $qb->select('a');
        if($paginated)
        {
            $first = ($offset * self::DEFAULT_BLOCKSIZE);
            $qb->setMaxResults(self::DEFAULT_BLOCKSIZE);
            $qb->setFirstResult($first);
        }

        return $qb->getQuery()->getResult();
    }

    public function countAll()
    {
        $qb = $this->createQueryBuilder('a');
        $qb->select('COUNT(a)');
        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * find company
     * @param $search_value
     * @param $column
     * @param $offset
     * @return Company []
     */
    public function findCompany($search_value, $column, $offset = 0)
    {
        $count = $query = $this->createQueryBuilder('a')
            ->where('a.'.$column.' LIKE :search_value')
            ->setParameter(
                'search_value', '%'.$search_value.'%'
            )
            ->select('count(a.id)')
            ->getQuery()
            ->getSingleScalarResult();
        $first = ($offset * self::DEFAULT_BLOCKSIZE);
        $query = $this->createQueryBuilder('a')
            ->where('a.'.$column.' LIKE :search_value')
            ->setParameter(
                'search_value', '%'.$search_value.'%'
            )
            ->setMaxResults(self::DEFAULT_BLOCKSIZE)
            ->setFirstResult($first);

        return [
            'TOTAL_AMOUNT' => $count,
            'RESULTS' => $query->getQuery()->getResult()
        ];
    }

    /**
     * @param Company $company
     * @return array
     */
    public function getData(Company $company) {
        $data = [
            'id' => $company->getId(),
            'nip' => $company->getNip(),
            'name' => $company->getName(),
            'address' => $company->getAddress(),
        ];

        return $data;
    }

}
