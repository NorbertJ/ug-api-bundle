<?php
namespace ApiBundle\Listener;

use Doctrine\Common\Util\Debug;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;

/**
 * Catch the JSON response and add developers data
 * 
 * @author Norbert Jurkiewicz <norbert.jurkiewicz@gmail.com>
 */
class AddDebugToResponseListener
{
    /**
     * @var string
     */
    private $environment;
    
    /**
     * 
     * @param FilterResponseEvent $event
     * @return void
     */
    public function onKernelResponse(FilterResponseEvent $event) {
        //add profiler URL
        $response = $event->getResponse();
        $responseType = $response->headers->get('content-type');
        if ($this->environment == 'dev' && $responseType == 'application/json') {
            $responseArray = json_decode($response->getContent(), true);
            $responseArray['_debug']['profilerUrl'] = $response->headers->get('X-Debug-Token-Link');
            $response->setContent(json_encode($responseArray));
        }
    }

    /**
     * @param UserService $userService
     */
    public function setEnvironment($environment) {
        $this->environment = $environment;
    }

}

